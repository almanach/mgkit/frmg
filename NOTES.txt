AFAIRE:

completer souscat pour refuser:
	  refuser +SCOMP       je refuse qu'il vienne
                  +DE-VCOMP    je refuse de venir


- Regarder construction groupe determinant: les trois
  Completer lexique sur cardinaux (deux trois ...)
  soit adj on determinant, soit arbre ancre par trois et categorisant det


- Regarder mechanisme pour bloquer / autoriser des ajunctions en
  ajoutant un argument adjctr
  par example, utilisation pour bloquer 2 adj de relatives
  pour permettre d'entrelacer adj et relative

  autoriser sur N2 adjctr=[-,rel,adj,prep] 
  - est pour no adj

  Certain objets bloquent adj: ex pronoun bloque adj et prep en ayant juste adjctr=[-,rel]
  un pronom comme 'ce' a une adj rel obligatoire: adjctr=[rel]

- Refuser adj equivalent a des participes passees.
  Autoriser adj de participiales sur N2
  une phrase ancree sur un verbe V est une participiale si v est participe + aux-req=etre

  Adj de participiale et part. present sont similaires

  DONE 24/08

- Ajouter adj pour adv nominaux de temps (il vient ce matin)
  utiliser trait 'time'
  (default time= - pour les noms)
  QUESTION: comment gerer des constructions comme: il est venu il y a moins de deux jours

  DONE 24/08

- Ajouter des arbres hors meta-grammaires pour gerer simple coordinations sur
  det: un ou des amis, ce sont mes ou tes amis 
  N: des jolies pommes et poire
  N2: des pommes et des poire
  adj: pommes vertes et rouges
  prep: je cherche sur et sous la table (seulement pour adj prep, pas prepobj)
  adv: je mange rapidemment et ferocement
  S: je mange et tu bois
     accord sur mode et wh

  DONE 24/08
  par extension de la meta-grammaires pour gerer quotes et star

- arbre pour convertir adj en noun quand determinant

- Mettre en place un mechanisme au debut de l'analyse pour nettoyer
  les infos du lexiques.
  => supprimer adj equivalent a un part. passe avec aux-req=etre
  => ? supprimer noms equivalent a adj
  
  DONE partiellement 24/08


- pb de syncro dans easy.pl quand on etend la longeur des mots composes

- pourquoi un pb avec la phrase
le ami que il propose de contr�ler vend une pomme
(relative + vcomp de-vcomp)

DONE 31/08

- pas possible pour l'instant de passer une disjonction de valeur sur lex

- extension pour checker les noeuds lex sur les champs lex ou lemma ?
  ou etendre coanchor et anchor pour pouvoir tester sur lex ou lemma ?

DONE 31/08: check on lex or lemma

- etendre les TAG dans DyALog pour acceder a la position dans la
  chaine avec $pos

DONE 31/08

- definir dans non terminaux speciaux pour tester 
  debut de phrase
  fin de pharse
  apres ou avant une ponctuation
de maniere a pouvoir definir la notion de virgule avant et apres un bloc de texte

DONE 31/08

- autoriser N2+participiale comme S-modifier
le chateau ayant brul�, ils partirent
le chateau d�truit, ils partirent

DONE

- voir le probl�mes des entit�es nomm�es: �ventuellement pr�voir
quelques arbres pour cela ou un pr�traitement avant Parsing pour
�tendre le treilli d'entr�e

DONE

- autoriser extraction de circonstanciel par adjonction

DONE

- examiner comment faire l'extraction de genitif sur sujet ou objet
de quel auteur Paul lit un livre

- examiner le cas de est-ce que pour les interrogatives
quels livres est-ce qu'il lit ?
qu'est-ce qu'il mange ?

Seulement utilisable pour les phrases principales (non dans les subordonnes)

- pb interrogative sur acomp:
  comment est-il ?
  comment va-t-il ?

- pb des inversion de sujet dans les interrogatives principales
  avec possibilite de duplication de sujet

  quels livres lit Jean ?
  quels livres Jean lit-il ?

- Mettre en place avant parsing la detection de nom propre (succession de noms propres)

- Regles de construction pour Det Titre NomPropre

DONE

- Clivee (?) de la forme
  C'est � cet homme que je veux parler
(� comparer avec C'est cet homme � qui je veux parler)

- pour les verbes support type (faire partie de, prendre conscience de, ...)
reflechir a une maniere de faire !

proposer un motif v +nc (+ prep)

- ajouter une entree lexique pour 'parce que' => csu wh +

DONE

- ajouter un trait � S pour l'extraction de maniere � �viter des
  infinitives avec extraction pour prep-vcomp (et autres cas �
  identifier: endroit ou on substitue S).

DONE

- supprimer du lexique entree: 
  * pour nc

- Completer lexique:

croire: <obj,acomp> <acomp>obj
s'appeler: ?
nommer: <obj,acomp>
se montrer: <acomp>+refl
continuer: <�-vcomp>
fermer: +refl
souvenir: <de-prepobj>+refl
essayer: <de-vcomp>

nomination: nc
fier de lui

- Typologie adverbes:

adv phrase avant advneg (ou verbe)
adv VP apres advneg (ou verbe)

pb sur classification adv aspect (souvent)

mode > oriente-sujet > evenement > negation > aspect > maniere

- Lexique 14/09/04

se montrer <acomp>
se faire <acomp>
trouver <obj,acomp>

monsieur utilise sans article:
	 ajouter trait +title sur
	 Monsieur, Madame, + pluriel
	 Docteur
	 Professeur
	 ...
essayer d'en trouver rapidemment une liste

utilisation comme incise (tete et fin de phrase, diverses positions
dans le corps: apres le/les verbes ?)

- Lexique 21/09/04

durer (comme mesurer, ...) : faux verbe transitif
completer les auxiliaires: para�tre, sembler

----------------------------------------------------------------------
LEXIQUES 09/10/04

Pb sur nouveau lexique Benoit:

   pr�senter: pas complet (reflexive et passif)
   prendre: pb de diathese sur passif
   ainsi que: coordination
   dementir: cat�gorisation
   comparer: <(obj),�-obj>
   partir: manque comme verbe
   am�liorer: pb de diathese sur passif
   bien: adj pl ?
   casser: pas complet (passif)

   fleur: verifier (adj,nc)

Modifier les entr�es pour
au-dedans        adv [pred='au-dedans', cat = adv] ;
au-dehors        adv [pred='au-dehors', cat = adv] ;
au-dessous       adv [pred='au-dessous', cat = adv] ;
au-dessus        adv [pred='au-dessus', cat = adv] ;
au-devant        adv [pred='au-devant', cat = adv] ;

car au est decompose en '� le/det/'
(ou alors est coherent sur les autres entree en '� le/det/-*')

----------------------------------------------------------------------
Grammaire 09/10/04

maximum input buffer length exceeded
SOLVED

----------------------------------------------------------------------
GRAMMAIRE 20/10/04

Un groupe N2 peut etre constitue par coordination de deux nc
exemple: 
	 Je devore livres et bouquins afin de m'instruire
	 Fruits et legumes sont deux aspects essentiels.


Construction en "S ou advneg"
	     il accepte ou non
	     il accepte ou pas

phrases courtes participiales
	en lisant

adv avec pp ou devant N2
    Les autres, notamment le criminologue Christian Pfeiffer, accusent l'�ducation collectiviste
ou lie a la coordination: 
   a donn� le ton et largement model� les principes de la formation.

   * coordination devenant relatives ?
   
   * la vigule d'incise peut disparaitre apres une coordination en tete de phrase
     Et dans la plupart des pays arabes, le r�alisme s'impose
     DONE

----------------------------------------------------------------------
Grammaire 8/11

* les relatives peuvent etre flottante en fin de phrase (sur modeles
  participiale) : accord avec sujet

  un homme viendra demain qui portera des fleurs

* permettre l'accroche de modifieurs du verbe (adv, PP, csu?) avec
  incise sur les arguments du verbe.

* adj categorisant sur scomp

* gerer la ponct ';' (sequence de S)

* 

----------------------------------------------------------------------
Lexique 8/11

nom de nationalite: les mettre egalement en adjectifs
en_tant_que: reclassifie en tant que 'prep' ?
la_plupart_du: idem

pb sur certain adv qui semble categoriser
   contrairement aux faits, ..
   independemment de ceci, 

terroriste: +adj
officiel: +adj
partiel: +adj
musulman: +adj
volontaire: +adj
agroalimentaire: +nc
paysan: +adj
travailleur: +adj

----------------------------------------------------------------------
Lexique 10/11/04

Modif v.p:
      * rester (rester +attr_sujet: rester beau)
      * demander (distran_a: demnder quelque chose a quelqu'un)


pb sur 
   devenu (aux_req=etre)
   lib�rale  +adj
   moderne +adj
   locale  +adj
   serbe   +adj
   russe   +adj
   palestinien +adj
   principal +adj
   occidental +adj

entendre +vcomp

BUG dans easy:syntax.pl (traitement aux_etre)
    SOLVED

----------------------------------------------------------------------
Syntaxe 11/11/04

* Adjoint de phrase (csu, adv, adv+time, prep) sur 'sujet' (autre que
cln) et 'aux'.
     SOLVED

* pronon sujet (type ce), ante-modifie par un 'sujet' (seulement sur clive ?)
  les fruits, ce sont ...
  communiquer, c'est ..

* ',' comme separateur de phrase

* Tester noms propres comme nom commun

* Pb sur clitique sujet inverse
  SOLVE

* Pb sur sujet inverse
  SOLVED

----------------------------------------------------------------------
Lexique 20/11/04

pb sur pas mal de verbes en @all�guer
   comme appr�cier

la plupart accepte des constructions en
   +vcomp
d'autre �galement 
	+de-vcomp (envisager)
	+�-vcomp  (appr�hender)
	SOVED (partially)

ils ne sont pas pronominaux

----------------------------------------------------------------------
Syntaxe 22/11/04

Ajouter regles dans easyforest pour les number ranges

Pb sur sujet inverse dans relatives sur non argument du verbe
   SOLVED

----------------------------------------------------------------------
Conversion EASY 23/11/04

Pb sur les sequences adj/number/nc: les trois jeunes filles

Le que de negation est pris comme adv
   il ne mange que des gateaux

Certain 'prep adj' ne semblent pas relies a leur verbe par CPL-V
	SOLVED (but maybe on aux)

Selon EASY, les sujets des participiales ne semblent pas marques (sauf
pour les part. presents)

Par contre les participiales sont indiquees en juxtaposition des principales !

Verifier regle de preference des adj vs nc en position comp (G2 E4)
	 SOLVED

Ajouter marque sujet des part. present quand adj sur N2
	
Renforcer le poids des constructions aux. (vs comp)
	  SOLVED

Pb de numerotations des positions sur les number
   ??

Pb sur ne_pas qui est lie dans Easy
       SOLVED in lexicon

Pb des attachements d'adv entre 'v' et 'acomp'
   sont tres verts / sont souvent verts
   info sur les type d'adverbes !
	SOLVED BUT TO BE TRIED


MOD-N entre participiales passe (present?) et GN modifie (pas de SUJ-V relation ?)
JUXT entre NV participale et NV principale (dans ordre occurrence?)

Quand adj transcategorise en N, MOD-N devient MOD-A

Detection de certain acronyme pour dectecter des MOD-N plutot que des JUXT ou APPOS
	  depute PS

COD pour v => ATT-SO/sujet quand verbe=etre

Transformer V+participe sans args en adj

COMP: entre csu et NV

Pb des csu_arg (scomp) sur adj

SUJ-V pour les controle (sujet et objet)
      elle lui demande d'�tre sage SUJ-V(lui,etre)


???? QUESTION: sujet/objet=ind dans 'cela rend fou'

defavoriser les sentences courtes
	    la m�re est fi�re de son fils

Rev�rifier la bonne penalisation de la transcategirisation, nom sans
det ou constituant discontigu
    mon tr�s cher ami

JUXT sur S coordonnee par ;

Pb sur extraction de forets dans le cas robuste


----------------------------------------------------------------------
Lexique 26/11

Laisser: <obj,vcomp>
	 laisser Paul entrer

mettre monsieur comme NP

----------------------------------------------------------------------
Syntaxe 26/11

reactiver coord entre adj antepose
	  SOLVED ?

ajouter regles pour tous (tous les enfants)
	SOLVED ?

tester pri combien (genitif)

ajouter r�gle pour nc suivi de nc ou NP
	salle machine
	SOLVED ?

pb sur partitif 'du'

pb sur construction comme 
   ' la solution retenue fut CELLE propos�e par Jean '
  (reprise sujet par CELLE, + generalement const 
  celle propose par Jean est-t-elle reconnue ?)
  SOLVED ?

Construction "ne vraiment pas"
	     adj 'adv' sur 'advneg'
	     SOLVED ?


S365 dans mondediplo10x20

----------------------------------------------------------------------
Lexique 1/12

aller:
	aller +vcomp: aller manger
avertir:
	+whcomp
intimer:
	+a-prepobj +(de-vcomp|scomp)

----------------------------------------------------------------------
Syntaxe

Exclamative en infinitive

	    Prendre de la farine.
	    Ne pas fumer.
	   
N2 construit par coord de 2 et + nc
   SOLVED (?)
N comme objet
  SOLVED (?)

comp doit doit accepter toutes les constructions de type N2 (en
particulier celles construit sur des pronoms ou des adj
transcategorises)

Mettre incise obligatoire pour csu sur VMod (mais pas sur S postpose)
       SOLVED (?)

? interpretation de 'd�s que' comme (prep+pronom relatif)
peut-on bloquer ce genre de choses

Coordination sur prepacomp: il passe pour idiot et pour stupide

L'extraction pour les clivee peut porter sur un modifier/cpl du verbe
	     c'est hier qu'elle est arriv�e
	     c'est d�s qu'elle a mang� qu'elle s'est sentie mieux
	     c'est souvent qu'elle parle



----------------------------------------------------------------------
Lexique 5/12

Pb sur les templates syntaxiques, certains a 4 ou 5 champs

a 4:
  abaisser
  autotransformer
  changer
  passer
  transformer


Trouver:
	+acomp +obj|scomp|de-prepvcomp
	trouver idiot qu'il vienne

----------------------------------------------------------------------
Portable Guillaume 06 631 681 09

----------------------------------------------------------------------
Examen corpus:

       accepter ':' comme ponctuation final (ou raccrocher les phrases separees par :)


       verifier construction N2 (Sigle)

       Les scomp peuvent etre quote dans les verbes de narration

Timeout sur cette phrase (1153:lemonde)

Il a propos� la mise � disposition de douches au stade de Gerland et la fourniture de bouteilles d' eau aux routiers .

(1312)
 Les fameuses taches de sang apparaissent , pr�c�dant de peu les
 hurlements fort �_propos_de la jeune com�dienne .

fail 1244          La loi , c' est la loi .
fail 1243          Et qui menacent d' envoyer les blind�s .
Phrase saturee etant des relatives (dans le monde)

fail 1232          En attendant qu' elles viennent de la base , faut surseoir .

fail 1223          DIVIS�E , la droite ?

Completer adv_bang pour accepter la categorie 'pres'

Completer le scanner DyALog pour sauter les 'euh'


Construction "- EASY: <S>"
Accepter construction S. S. S. (sur modele S; S; S; S.)
Eventuellement

Adjonction de participiales sur N2 non satures

----------------------------------------------------------------------
12/12/04 Analyse erreurs 1st round

mlcc:
'(' ')' '-' 'Monsieur' pr�sident M_. c' : son nos mise entre je qu'

lemonde:
( ) pour sur : M_. son qu' comme ses cette leur on " mais

litteraire_4
; M - sa de_la lui sur avec Laure vicomte mais me Gaspard ma monsieur
toute jamais fille -il -je dont �cria S' ils Mademoiselle

litteraire_3
s' qu' :  " me on ... ! - m�me leur tous encore autre homme

litteraire_2
je est ce M Seigneulles chevalier -t-il moi deux -elle �cria c' Georgette
Mademoiselle p�re r�pondit -je ma t�te m' rien S' Reine nous votre
main tous leurs tu gens cela monsieur(1) dire mieux(1) oui Reprit(1)
Monsieur mal

litteraire_1
qui qu' son " on comme mon leur ma ses m' toute tout ce s' vous vie 
encore : ... leurs dont homme quand mes ils peut aujourd'_hui pauvres
l� ceux grand France ann�e malheureux -vous pens�e semble(1)

questions_trec
Qu' qu' Nommez temps ( ) expression nomm� continent comt� Sur 

questions_amaryllis
) � � se qu' questions ( -ce r�pondre -t-elle comme il peuvent '2' ;
elle autres m�me milieu On expression suivantes non type trois nous
propose(1) ils d�bat programme '3' probl�me encore pourquoi(1) toutes
existe celui �volution image �valuation sous syst�me celle

mail_8 
vous ( ) pour ~(1) " je votre ce - il ; * j' euros on option recherche
programme information cela = bient�t(1) soit(1) surtout deux
informatique format � � recevoir pense s�minaire(1) carte prochaine
cliquez compte o� m�me cordialement(1) oui(1)

----------------------------------------------------------------------
Lexique

dissocier
	+refl de-prepobj

Ajouter qu' et Qu' comme advneg

----------------------------------------------------------------------
Syntaxe

Accepter des imperatives terminee par '.' (questions_trec)

V�rifier  'acomp' accept� dans les constructions passive

Construction cliv�e "Qu'est-ce que Java" (questions_trec)

Accepter des �numerations de phrase (comme ;) mais avec ','

Construction: Monsieur le pr�sident (general_mlcc)

accepter ... comme starter de phrase

verifier construction wh non argument du verbe

mettre sous comp les m�mes construction que N2

Mettre les attachements prep sur N2 plutot que N (car acceptable sur les pronoms)
celle au chapeau

sur adj_on_det: r�fuire arg0.real

----------------------------------------------------------------------
Conversion EASy


Favoriser construction auxiliaire

Mettre genitif en COD

Mettre les pri en CMP (combien)

privilligie adj sur v pour participes passe

verifier construction range 'de dix � quinze gar�ons'


regarder construction 'autour_de' qui devraient etre englobe dans le GP

Reprendre cliv�es

Mettre locatif en CPL-V

GP as Modifier adj


favoriser 'pas' comme negation et non comme nom dans les phrases negative

----------------------------------------------------------------------
Syntaxe 13/12

Verifier adj csu sur sujet (a traver VSubj)

----------------------------------------------------------------------

Mette les prel de circonstancielles comme les pri 
      (o�, pour lesquelles, ...)


v�rifier les 'que' de n�gation comme GR et mod V

encore des pb de d�calages dans le cas de litteraire_4:7
du a utilisation de _ comme separateur !

======================================================================
18/10/07 easyforest

* Mieux gerer la desambiguation en mode robuste pour eviter les blocs trop petits.

* Une coord peut demarrer une phrase pour EASy

Dans FRMG verifier possibilite de modifier une prep par un adverbe
peu apr�s sa venue

* Verifier que les relations sont ciblee sur le coordonnant quand
  existence COORD.


* [lefff] manque "lui �tre �gal"


* Les pronoms peuvent �tre modifi�s par un adjectif ant�pos� "chers tous" "chers vous"
mais cela doit �tre plut�t rare

* Corrig�: ATB-SO quand comp pointe sur un N2

* ATB-SO sur "son objectif [est] [de Sinf]"

* V�rifier construction attributive sur "elle l'est" 'l' est un attribut en forme clitique
elle est jolie -> elle l'est.

* Eliminer degree sur adj

* Ajouter mode sur adj avec mode=participle => position=post

243 122 41 14  5 2 1
    121 81 27  9 3 1            
        30 53 18 6 2
          -23 45 12 4
                    
     81 43 14 5 2 1

    108 50 17  6 3 1
        27 14  5 2 1
            9  5 2 1

======================================================================
Dans TSNLP

pb around 1322 (ou 1323)

 L'ing�nieur sera averti par l'entreprise si le conseil accepte ou non.
 L'ing�nieur obtient l'accord de l'entreprise, rapidement, pour venir.
L'entreprise est remerci� pour le soutien.
 
Il veut ne rien voir.
 => (cause) rien est un pro (pas un advneg)

 Arriv�-je?
 => sxpipe:  donne participe sur arriv�

Beaucoup d'entre vous viennent.
=> construction avec 'entre'
=> peut-etre ajout systematique pour les pre-det

Elle et lui viennent.
 Elle et moi venons.
 Geind-on?
Il est arriv� une femme.
Il faut diriger l'entreprise.
 Il faut venir et partir.
 L'espoir ou le regret que la femme vienne est �vident.
 L'ing�nieur accepte maintes entreprises.
 => maint n'est pas utilisable comme det (mais adj)

 L'ing�nieur te l'apprend.
 La crainte de venir et de combattre est �vident.
 Neige-t-il?
 Qui est-ce qui appelle?
 Toi et moi venons.
 Viens-tu?
Il a fait beau.
Il a fallu diriger l'entreprise.
 Il a �t� d�cid� que l'ing�nieur viendrait.
Il avoue n'avoir pas fait d�marrer le moteur.
Il avoue ne l'avoir pas fait d�marrer.
 L'ing�nieur a, le soir, montr� le plan au conseil.
 L'ing�nieur sera averti par l'entreprise si le conseil accepte ou non.


De F.Role (07/09)
Pb de desambiguisation dans
Le terme d�signe un animal qui mange des pommes


Demande-le-moi
=> FRMG incorrect sur les clitiques post-verbaux (cla < cld)
   pas de notion de cld[13] < cla

Dem

======================================================================
�t� 2009

[Lefff] lui peser 
[Lefff] il est convenu que (impersonnel avec �tre)

[Sxpipe] 'Cours de Justice' Ne laisse pas une lecture avec les mots pris comme nc,
ie cours de justice

[sxpipe] 'neuf' retourne seulement le _NUMBER et pas l'adjectif !

[sxpipe] regarder si on peut aussi enlever certain POS pour _NUMBER

[frmg] 'tout' peut etre un adv modifiant un adj
doit lutter contre 'tout' vu comme un determinant (categorie ferm�e)

OK sur proposition ML: 'tout' dans le contexte  de comp est un adv

[conversion] pb sur conversion des _NUMBER quand ils sont adj (donne un mauvais GN)

[frmg] constructions
       si Adj/Adv que S
       tellement Adj/Adv que S
DONE

[frmg] comment interpreter
       etes-vous [tous] [dispo]
       quel est le role de tous dans la phrase

[frmg] superlative peuvent bloquer des adjonction
       la plus courte possible et fonction ...

[sxpipe] Union europ�enne
         Commission europ�enne
	 Parlement europ�en

[frmg] certaines adj sont bloquees dans
       la banque allemande Deutsche Bank
        l'ancien chef milicien Paul Touvier
DONE

[sxpipe] 'janvier dernier' donne un bloc pour sxpipe


[frmg/lefff] sous-cat scomp pour certains noms:
	     fait, motif, souhait, d�sir, id�e, 
	     affirmation, d�claration, indication, accord, 
	     impression, envie, sorte, constat, rappel,
	     sentiment, proposition,  croyance, assurance,
	     crainte, inqui�tude, pens�e, doute, condition,
	     annonce, 

----------
comme + S, modif GN ou GA

status de 'plus de'
       plus d'une semaine
       plus de trois jours
       
genre de adv[+modnc]
      groupant: environ, plus_ou_moins, au plus, au moins, moins de
      au max., au min.,

tout �a, tout ceci, tout cela
     tout modifiant un pronom
     nous tous, nous seuls, eux seuls ...
     tous ceux (qui S|PP)
     toutes celles (qui S|PP)
     seuls ceux (...)
     seuls ceux-ci
     seuls celles-l�
     beaucoup de ceux (-ci|-l�|S|PP)

VMod au dessus de prel et pri, dans les extractions

Mieux bloquer la lecture de short sentences (ne pas toujour passer en
phrases compl�tes incoh�rente)
	penaliser imperatif
	penaliser 'l�' pour La (g�n�raliser)


enlever politiquer comme verbe

'c'est X Y' pour Y est X
c'est beau la mer. 
c'est loin la mer.
(sadv pour �tre)


======================================================================
23/09/09 avec Marie-Laure

* script de cr�ation matrice confusion pour un mot donn� (+cat)

* participe pass� en position 'comp' : il le trouve r�g�n�r�

* statut de 'ce' (dans ce � quoi il s'attend; ce � diff�rents niveaux)

* parall�lisme de structure dans les coordinations (favoriser)
  t�moins de leur folie et maintenant t�moin de leur bonheur

* les plus connus: favoriser lecture en adv pour 'plus'
  plus subtil: il est (le plus)_GR beau_GA
  (le plus beau)_GN mange
  (le plus beau)_GN qu'on n'ait jamais vu mange


* v�rifier les modifieurs sur un adj transcat�goris�


* v�rifier les CleftQue qui ne peuvent pas �tre autonomes
  pour ne citer que les plus connus que je connaisse.

* donner une liste de mots preferentiellement en adjectif
  petit, cher, grand, bon, premier, dernier, prochain, 
  jeune, pauvre, vieil, beau, nouveau, ancien, gros
  principal
  m�me
  + adj numeraux (mille francs)

donateur: pas adj

* d�favoriser number comme pro

* faire des PV dans 'de tout manger' 'de vraiment manger'

* pour les predet (et det) mettre l'accord avec N2.top plutot qu'avec nc ou N2.bot

* ?? predet sur les noms massifs
  relacher les contraintes pour accepter des singuliers

* p�naliser trop comme 'adj'

* ajouter 'terre � terre' comme adj dans Lefff

* cas de 'loin' adv
  dans les cas ou il est attribut (c'est loin) ? Pas pr�vu par la sous-cat de �tre  

* 'ne pas aller loin' comme verbe support dans Lefff ?

* coordination sur predet
  tant de regret et de nostalgie

* favoriser det plutot que prep+det apres n�gation (advneg)
  que_restr + des => det

* cas de 'ni plus ni moins'

======================================================================
2011/05/12

* verbes autorisant un sujet impersonnel (et un sujet profond) peuvent
  appara�tre sans sujet impersonnel et sujet profond invers�
  - pour des  cas non pronominaux
 

     reste le probl�me soulev� par Paul
     demeure en suspens la question de Paul
     
     il se raconte que
     *? se raconte que 

* toutes sortes de pb pour contr�ler les adjonctions sur S
  correspondant � 
    - verbes � contr�le (ctrlaux) dans certains cas
      que [d�cide-t-il de] manger ?
    - extraction de modifieurs (xmdoaux) 
      [o�] mange-t-il
    - est-ce que (estceaux)
      [est-ce qu'] il mange
      qu'[est-ce qu'] il mange ?

 le pb est la possible composition de ces divers aux, et dans quel
 contextes les adj sont faites

      - ctrlaux est it�rable sur lui-m�me
      - ctrlaux n'est possible que dans les cas o� on une extraction
        (d'un arg ou d'un mod par xmodaux)
      - xmodaux n'est pas it�rable
      - xmodaux n'est pas utilisable dans le contexte d'une extraction
        d'un arg
      - xmodaux est possible avant ctrlaux dans le contexte d'une
        phrase canonique
      - estceaux est possible devant ctrlaux
      - estceaux est non it�rable 
      - estceaux ne peut pr�ceder xmodaux
      - xmodaux peut pr�cder estcaux
      - estceaux seulement possible pour wh-sentences


      xmodaux < estceaux* < ctrlaux* on canonical
                estceaux* < ctrlaux* on arg_extract

                       S.top         S.bot
  canonical             -              - 
                      wh|rel|cleft|topic    adjx
  verb_extract_wh           estceaux|adjx   adjx
  verb_extract_rel             adjx         adjx
  verb_extract_cleft           adjx         adjx
  verb_extract_topic           adjx         adjx

               root.top         --->    foot.bot
 ctrlaux        adjx                      adjx
 estceaux       estceaux|-                adjx
  estcaux-cleft    adjx                   cleft    *** tmp: to check
 xmodaux        wh|rel|cleft|topic        adjx
                    wh                    estceaux


======================================================================
2011-05-13

* manque extraction sur adjectif dans relatives (alors que cela marche
  pour les wh)

    le livre dont tu es fier

======================================================================
2011-07-13

* cordinnation entre adv temporal et noms temporal
eg: Je serai l� demain et vendredi
DONE

* expression: il y a ap�ro, il y a belote, ...
  pas d'article !
  eg: y-a-t-il relation de cause
DONE

* dans SxPipe: ;) comme SMILEY

* SVP comme abbrev pour "s'il vous plait"

* Accepter des xcomp compl�tive sans le 'que'
  je lui dis je veux venir

* modifieur sur presentation
  OK pour moi
  h�las pour moi
DONE

* correction de ou en o� 

* v�rifier sembler comme verbe avec Att
DONE

* prep avec ellipse: je vote pour
DONE

* utilisation de 'le premier' et consort
  eg: il la mangea le premier
DONE

* v�rifier juger comme intransitif
  eg: le tribunal jugera
DONE

* Merci de X

* oh comme adv et pas comme pres
  Oh Paul, que manges tu ?
OK

* phrases d�marrant par un nombre (arabe ou romain)

* 'comme �tant mes amis'

* 'en quoi X'
  eg: c'est quoi ta proposition

* se_savoir + Att
  se savoir jolie
  se savoir riche
DONE

* phrase courte:
  peut-�tre la fuite

* phrases entre parenth�ses
  ( il vient, non ? )

* dire plus whcomp
  en fait pb sur 'dites-moi <whcomp>' imp�rative

* tenir quitte
DONE

* s'�tonner que
<litteraire_4.E1295> Je m' �tonne qu' un gar�on d' esprit comme vous ait en politique des id�es si fausses .


* expression: "on ne peut plus" comme adv intensifieur
les adversaires du FIS sont on ne peut plus divis�s.

* v�rifier lctag pour les cat qui n'ont pas de tag_feature (comme
  clneg)
  pb sur follow_coord qui manque le cas clneg
DONE

* construction '<short sentence>, <coo> S'
Quelques jours encore , et M Levrault mettait le pied sur la terre
promise .
DONE

* VMod apres prep
pour, par exemple, r�ussir dans la vie
DONE

* coord entre PP et CS
Elles sont rencontr�es g�n�ralement en_fin_d' �volution ou lorsqu' il existe des troubles cognitifs .
DONE

* Att en position wh peut �tre un PP
De quelle couleur devient le papier de tournesol ? 
DONE

* coord sur des relatives avec PP
et rien n' �tait plus fantastique que cette mer blanche, d'o�
jaillissaient et d'o� plongeaient des oiseaux.

* construction de la forme "c'est vouloir aggraver ma faute"
ie "c'est vouloir .."

* construction "Donnez-moi de quoi fonder la v�rit� sociale"

* �num�ration en apposition avec insise stricte
Actuellement on peut consid�rer que seuls les troubles moteurs  (  akin�sie , rigidit� , tremblement  )  sont accessibles � la th�rapeutique .

* strange pb sur "au loin , des per�ants �clats troublaient la paix"

* ajout S_enum

* pour les incise dash _ _, une virgule peut �tre pr�sente
Avec 1,6 million d' unit�s vendues en 1991 _ en baisse de 20,7 % , _
le march� outre-Manche a ainsi recul� de 30,8 % en deux ans .
DONE

* expression "c�te � c�te" comme adv
On vendangea c�te � c�te

* coord entre adj et relative
Un cursus classique mais qui n' est plus unique .

* disloqu�e simple sur sujet: "X c'est ..."
Son champ d' action c' est le monde entier .

* construction "X oblige, .."
Rigueur oblige , m�c�nes et sponsors recomptent leurs deniers .
DONE

* construction "c'est pourquoi X"
C' est pourquoi Alonso se retrouve confin� sur 1 hectare .

* modifieur "autre que"
Son d�part de l' Union aurait un caract�re autre que symbolique .

* expression "mal leur en a pris"

* modifeurs multiples de types diff�rents sur VMod (ou S)
le chat dort avec lui ce soir

* construction unit� "<number> <unit� modait> <unit�>
L' arabica s' �tablit � 1700 dollars la tonne . 
DONE

* construction "mieux encore" ("plus encore")
Et , mieux encore , je vous souhaite une meilleure sant� �conomique .

* construction adj+sujet profond invers� mais sans aux '�tre' dans des
short sentences (a rapprocher de short sentence passives: termin�es
les vacances)
Difficile , dans ces conditions , d' envisager une am�lioration rapide

* inversion d'adv dans des g�rondifs (ou adj)
L' id�e , tentante politiquement , ne s�duit pas.
inversion possible dans des cas d'incises
DONE

* supermod sur "davantage de d�tracteurs virulents que de fervents
  partisans"
DONE

* rev�rifier les comparatives
il mange autant de pommes que lui. 
il mange autant les pommes que lui. 
DONE

* expression "voici X" comme adv temporel (sans marques d'incises)
Le Nigeria , qui �tait voici encore trente ans un grand pays agricole
, a progressivement abandonn� ses cultures pour vivre uniquement du
p�trole .
assez similaire � "il y a"
DONE

* construction "�tant donn� X"
Ces propositions sont inacceptables �tant donn�es les in�galit�s

* forme d'adv temporel "mais trop tard" "mais un peu tard"
Les politiques comprennent , mais un peu tard , qu' ils sont manipul�s par la t�l�vision
"mais" comme modifieur de certains adv (mais comme adv)

* construction "tel que"
Il y a en effet bien longtemps que ce dialogue tripartite , tel que l' avait con�u Harold Macmillan en 1962 , �tait devenu un dialogue de sourds .

* Pb sur des inversions de sujets dans des relatives avec xcomp
C' est un triste spectacle que vient de donner Paul. 

* construction short sentence avec infinitive et sujet prep
de quoi pavoiser 
pas de quoi pavoiser 
de quoi tenir jusqu'aux �lections

* plus g�n�ralement sur 'quoi' et 'de quoi'
- des expressions comme 'faute de quoi' 'en �chande de quoi' 'en
cons�quence de quoi' + S(not inf)
- de quoi + S(inf)
- de quoi comme partitif wh: de quoi mange-t-il ?





