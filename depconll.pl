/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2015 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  depconll.pl -- DepConll format
 *
 * ----------------------------------------------------------------
 * Description
 *   Native schema for FRMG, but emitted in a tabular format similar to CONLL (-depconll)
 * ----------------------------------------------------------------
 */

:-include 'header.tag'.
:-require 'conll.pl'.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DepConll
	  
:-std_prolog depconll_ids/2,depconll_emit/1.
:-extensional depconll2node/2, node2depconll/2.

depconll_emit :-
	emit_multi('DEPCONLL'),
	depconll_ids(0,1),
	sentence(SId),
	recorded(mode(Mode)),
	( recorded(has_best_parse) ->
	  Best = yes
	;
	  Best = no
	),
	format('## sentence=~w mode=~w best=~w\n',[SId,Mode,Best]),
	%% TO BE IMPLEMENTED
	depconll_emit(1),
	true
	.


depconll_ids(Left,Pos) :-
%	format('depconll_ids left=~w pos=~w\n',[Left,Pos]),
	( N::node{ id => NId,
		   lemma => Lemma,
		   cat => Cat,
		   cluster => cluster{ left => Left, right => Right }
		 },
	  \+ node2depconll(NId,_),
%	  format('test depconll_ids left=~w pos=~w right=~w nid=~w cat=~w lemma=~w\n',[Left,Pos,Right,NId,Cat,Lemma]),
	  NId \== root,
	  \+ ( E::edge{ source => node{ id => _NId,
					cluster => cluster{ left => Left, right => Left }
				      },
			target => N
		      },
	       _NId \== NId,
	       _NId \== root,
	       \+ node2depconll(_NId,_),
%	       format('fail because of _nid=~w edge=~w\n',[_NId,E]),
	       true
	     ) ->
%	  format('select depconll_ids left=~w pos=~w right=~w nid=~w\n',[Left,Pos,Right,NId]),
	  record( depconll2node(Pos,NId) ),
	  record( node2depconll(NId,Pos) ),
	  XPos is Pos + 1,
	  depconll_ids(Right,XPos)
	;
	  true
	)
	.

depconll_emit(Pos) :-
	( depconll2node(Pos,NId) ->
	  N::node{ id => NId,
		 lemma => _Lemma,
		   cat => Cat,
		   form => _Form,
		   cluster => cluster{ lex => TokenId, token => _Token, left => Left, right => Right }
		 },
	  (_Token = '' -> Token = '_' ; Token = _Token ),
	  (_Lemma = '' -> Lemma = '_' ; Lemma = _Lemma ),
	  ( TokenId = '' ->
	    Tokens = ['_'],
	    TIds = ['_']
	  ; TokenId = [_|_] ->
	    depconll_tokens(TokenId,Tokens),
	    TIds = TokenId
	  ;
	    depconll_tokens([TokenId],Tokens),
	    TIds = [TokenId]
	  ),
	  (Tokens = [] -> XTokens = ['_'] ; XTokens = Tokens ),
	  mutable(MSPos,[]),
	  mutable(MLabels,[]),
	  every((
		 edge{ source => _N::node{ id => _SId },
		       target => node{ cluster => cluster{ left => Left, right => Right }},
		       type => edge_kind[~ virtual],
		       label => _Label },
				%	       format('test nid=~w _N=~w\n',[NId,_N]),
		 node2depconll(_SId,_SPos),
		 mutable_list_extend(MSPos,_SPos),
		 mutable_list_extend(MLabels,_Label)
		)),
	  mutable_read(MSPos,_LSPos),
	  mutable_read(MLabels,_LLabels),
	  (_LSPos = [_|_] ->
	   LSPos = _LSPos,
	   LLabels = _LLabels
	  ;
	   LSPos = [0],
	   LLabels = [root]
	  ),
	  
	  ( node2op(NId,OId),
	    recorded(op{ id => OId, top => Top })
	  xor
	  recorded(node2top(NId,Top))
	  xor
	  Top =[],
	    OId = '_'
	  ),
	  ( Cat = cat[v,aux] ->
	    conll_fullcat(Cat,'V',OId,N,FullCat,_)
	  ;
	    FullCat = conll_fullcat[~ ['V','VINF','VPP','VS','VIMP','VPR']]
	  ),
	  mutable(MFeat,[]),
	  every((
	       %%		 format('nid=~w top=~w\n',[NId,Top]),
		 Top \== [],
		 domain(F,[tense,person,number,mode]),
		 inlined_feature_arg(Top,F,_,V),
				%	       \+ (F=mode, FullCat = conll_fullcat['VPP','VPR']),
		 \+ ( F=tense,
		      FullCat \== conll_fullcat['VPP','VPR'],
		      (inlined_feature_arg(Top,_Mode,_,conditional), _Mode = mode)),
		 \+ ( Cat=cat[adv,advneg,prep],
		      domain(F,[number,gender,person])
		    ),
		 feature2depconll(F,V,FV,FullCat),
		 mutable_list_extend(MFeat,FV)
		)),
	  mutable_read(MFeat,_MSTag),
	  conll_fvlist2fv(_MSTag,MSTag),
	  
	  format('~w\t~L\t~w\t~w\t~w\t~L\t~L\t~w\t~w\t~L\n',
		 [Pos,
		  ['~w','_'],XTokens,
		  Lemma,
		  Cat,
		  MSTag,		% FS
		  ['~w','|'],LSPos,
		  ['~w','|'],LLabels,
		  '_',		% non proj deps
		  '_',
		  ['~w','+'],TIds
		 ]),
	  XPos is Pos + 1,
	  depconll_emit(XPos)
	;
	  true
	)
	.

:-std_prolog depconll_tokens/2.

depconll_tokens(LTokenIds,LTokens) :-
	( LTokenIds = [] ->
	  LTokens = []
	;
	  LTokenIds = [TokenId|LTokenIds2],
	  ( recorded(depconll_emited_token(TokenId)) ->
	    depconll_tokens(LTokenIds2,LTokens)
	  ;
	    'T'(TokenId,Token),
	    record(depconll_emited_token(TokenId)),
	    depconll_tokens(LTokenIds2,LTokens2),
	    LTokens = [Token|LTokens2]
	  )
	)
	.

:-extensional feature2depconll/4.

feature2depconll(gender,masc,'g=m',conll_fullcat[~ ['VPR','VINF','CC','CS']]).
feature2depconll(gender,fem,'g=f',conll_fullcat[~ ['VPR','VINF','CC','CS']]).

feature2depconll(number,pl,'n=p',conll_fullcat[~ ['VPR','VINF','CC','CS']]).
feature2depconll(number,sg,'n=s',conll_fullcat[~ ['VPR','VINF','CC','CS']]).

feature2depconll(person,1,'p=1',conll_fullcat[~ ['NC','NP','ADJ','VPR','VPP','VINF','CC','CS']]).
feature2depconll(person,2,'p=2',conll_fullcat[~ ['NC','NP','ADJ','VPR','VPP','VINF','CC','CS']]).
feature2depconll(person,3,'p=3',conll_fullcat[~ ['NC','NP','ADJ','VPR','VPP','VINF','CC','CS']]).

feature2depconll(tense,future,'t=fut',conll_fullcat['V','VS','VIMP']).
feature2depconll(tense,'future-perfect','t=fut',conll_fullcat['V','VS','VIMP']).
feature2depconll(tense,present,'t=pst',conll_fullcat['V','VS','VIMP','VPR']).
feature2depconll(tense,past,'t=past',conll_fullcat['V','VS','VIMP']).
feature2depconll(tense,'past-historic','t=past',conll_fullcat['V','VS','VIMP']).
feature2depconll(tense,'participle','t=past',conll_fullcat['V','VS','VIMP']).
feature2depconll(tense,_,'t=past',conll_fullcat['VPP']).
feature2depconll(tense,imperfect,'t=impft',conll_fullcat['V','VS','VIMP']).

feature2depconll(mode,conditional,'t=cond',conll_fullcat['V']).

feature2depconll(mode,indicative,'m=ind',conll_fullcat['V']).
feature2depconll(mode,subjonctive,'m=subj',conll_fullcat['VS']).
feature2depconll(mode,_,'m=inf',conll_fullcat['VINF']).
feature2depconll(mode,_,'m=part',conll_fullcat['VPP','VPR']).
feature2depconll(mode,_,'m=imp',conll_fullcat['VIMP']).

feature2depconll(type,card,'s=card',_).
feature2depconll(type,def,'s=def',_).
%feature2depconll(type,dem,'s=card').
feature2depconll(type,excl,'s=excl',_).
feature2depconll(type,ind,'s=ind',_).
feature2depconll(type,int,'s=int',_).
feature2depconll(type,ord,'s=ord',_).
feature2depconll(type,part,'s=part',_).
feature2depconll(type,poss,'s=poss',_).
feature2depconll(type,qual,'s=qual',_).
feature2depconll(type,rel,'s=rel',_).
