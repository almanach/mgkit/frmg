/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2015 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  transform.pl -- Edge transformation
 *
 * ----------------------------------------------------------------
 * Description
 *  Some edges may be renamed, displaced, ...
 *  This file provides a set of such transformation rules
 * ----------------------------------------------------------------
 */

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Edge transformations

:-include 'header.tag'.
:-require 'best.pl'.

:-std_prolog edge_reroot/2.

edge_reroot(E::edge{ id => EId,
		     source => Source::node{ id => SId },
		     target => Target::node{ id => TId },
		     type => Type,
		     label => Label,
		     deriv => Derivs,
		     secondary => Secondary
		   },
	    Reroot::node{ id => Rerooted_From_Id }
	   ) :-
	( node!empty(Source),
	  edge{ source => XSource, target => Source }
	->
	  edge_reroot(edge{ id => EId,
			    source => XSource,
			    target => Target,
			    type => Type,
			    label => Label,
			    deriv => Derivs,
			    secondary => Secondary
			  },
		      Reroot)
	 ;
	  Secondary ?= [],
	  record(E),
	  alive_deriv(Derivs,Deriv),
	  deriv(Deriv,EId,Span,_,_),
	  record( reroot_source(EId,Span,SId,Rerooted_From_Id) ),
	  record_without_doublon(reroot_loop),
	  true
	)
	.

:-rec_prolog edge_transform/0.

edge_transform :-
	\+ opt(conll),
	E::edge{ source => V::node{ cat => v, id => NId },
		 target => Cl::node{ cat => cld[] },
		 type => lexical,
		 label => preparg,
		 deriv => Derivs,
		 id => EId
	       },
	chain( V >> (subst @ object) >> Obj::node{} ),
	\+ ( node2live_ht(NId,HTId),
	     check_arg_feature(HTId,args[arg1,arg2],function,obj�)
	   ),
	erase(E),
	edge_reroot( edge{ id => EId,
			   source => Obj,
			   target => Cl,
			   label => genitive,
			   type => lexical,
			   deriv => Derivs
			 },
		     V
		   )
	.
	
edge_transform :-
	E::edge{ source => V::node{ cat => v },
		 target => Cl::node{ cat => clg },
		 type => lexical,
		 label => clg,
		 id => EId,
		 deriv => Derivs
	       },
	chain( V >> (subst @ object ) >> Obj::node{} ),
	erase(E),
	edge_reroot( edge{ id => EId,
			   source => Obj,
			   target => Cl,
			   label => genitive,
			   type => lexical,
			   deriv => Derivs
			 },
		     V
		   )
	.

edge_transform :-
	\+ opt(conll),
	E::edge{ source => Aux::node{ cat => aux },
		 target => Subj,
		 type => lexical,
		 label => subject,
		 id => EId,
		 deriv => Derivs
	       },
	aux2main(Aux,V),
	Aux \== V,
	erase(E),
	edge_reroot( edge{ source => V,
			   target => Subj,
			   type => lexical,
			   label => subject,
			   id => EId,
			   deriv => Derivs
			 },
		     Aux
		   )
	.

:-std_prolog aux2main/2.

aux2main(Aux,V) :-
	( chain(Aux::node{ cat => cat[aux,v] } << (adj @ label['Infl',aux]) << _V::node{}) ->
	  aux2main(_V,V)
	;
	  V = Aux
	)
	.

edge_transform :-
	E::edge{ source => N::node{},
		 target => Incise::node{ cat => incise },
		 type => adj,
		 label => incise
	       },
	\+ node!empty(N),
	erase(Incise),
	erase(E),
	every(( E2::edge{ source => Incise,
			  target => InciseMark,
			  type => lexical,
			  label => void,
			  deriv => Derivs2,
			  id => EId2
			},
		erase(E2),
		edge_reroot( edge{ source => N,
				   target => InciseMark,
				   label => void,
				   type => lexical,
				   deriv => Derivs2,
				   id => EId2
				 },
			     Incise
			   )
	      ))
	.

%% quotes
edge_transform :-
	E::edge{ source => N::node{},
		 target => M::node{ tree => MTree },
		 type => adj
	       },
	\+ node!empty(N),
	node!empty(M),
	( domain(K,quoted[double_quoted,simple_quoted,chevron_quoted,plus_quoted]),
	  domain(K,MTree)
	xor
	  fail
	),
	erase(M),
	erase(E),
	every(( E2::edge{ source => M,
			  target => QuoteMark,
			  type => lexical,
			  label => void,
			  deriv => Derivs2,
			  id => EId2
			},
		erase(E2),
		edge_reroot( edge{ source => N,
				   target => QuoteMark,
				   label => void,
				   type => lexical,
				   deriv => Derivs2,
				   id => EId2
				 },
			     M
			   )
	      ))
	.


edge_transform :-
	E::edge{ source => _V::node{},
		 type => E_Mode::edge_kind[adj],
		 label => E_Label,
		 target => Mod::node{}
	       },
	E1::edge{ id => EId1,
		  source => Mod,
		  target => Target::node{ cat => end },
		  type => subst
		},
	node!empty(Mod),
	\+ ( edge{ source => Mod,
		   id => _EId1
		 },
	     EId1 \== _EId1
	   ),
	erase(E),
	erase(E1),
	erase(Target),
	erase(Mod)
	.
	
edge_transform :-
	E::edge{ source => _V::node{ cat => _VCat},
		 type => E_Mode::edge_kind[adj,subst],
		 label => E_Label,
		 target => Mod::node{ tree => TTree, xcat => TXCat },
		 deriv => TDerivs
	       },
	node!empty(Mod),
	(\+ node!empty(_V)
	xor _VCat = comp 	% short sentences
	),
	( chain( Mod >> subst >> ModAnchor::node{ cat => ModAnchorCat, cluster => cluster{ left => Left, right => Right }} ),
	  (Right > Left xor \+ edge{ source => ModAnchor } ),
	  \+ ModAnchorCat = start,
	  \+ ModAnchorCat = end,
	  \+ ( chain( Mod >> subst >> node{ cluster => cluster{ left => _Left } }),
	       _Left < Left
	     )
	->
	  Mode = subst,
	  true
	; chain( Mod >> lexical >> ModAnchor ),
	  Right > Left,
	  \+ ( chain( Mod >> lexical >> node{ cluster => cluster{ left => _Left } }),
	       _Left < Left
	     )
	->
	  Mode = lexical,
	  true
% 	  \+ ( chain( Mod >> _ >> _ModAnchor::node{ cluster => cluster{ right => _Right }} ),
% %	       _Right =< Left
% 	       \+ ModAnchor = _ModAnchor
% 	     ) ->
% 	  true
 	;
 	  fail
	),
%	format('reroot at ~w\n',[E]),
	erase(E),
	erase(Mod),
	( aux2main(_V,V) xor _V = V ),
	every(( E1::edge{ source => Mod,
			  target => Target::node{ cat => TargetCat},
			  label => Label,
			  type => Type,
			  deriv => Derivs,
			  id => EId1
			},
		erase(E1),
		( Target = ModAnchor ->
		  ( E_Mode = subst ->
		    XLabel = E_Label
		  ; Mode = subst ->
		    XLabel = Label
		  ;
		    XLabel = E_Label
		  ),
		  edge_reroot( edge{ source => V,
				     target => ModAnchor,
				     label => XLabel,
				     type => E_Mode,
				     deriv => Derivs,
				     id => EId1
				   },
			       Mod
			     )
		; domain(TargetCat,[start,end]) ->
		  erase(Target)
		; Label = incise,
		  Type = adj ->
%		  format('reroot incise at ~w\n',[Target]),
		  erase(Target),
		  every(( E2::edge{ source => Target,
				    target => InciseMark,
				    label => void,
				    type => lexical,
				    deriv => Derivs2,
				    id => EId2
				  },
			  erase(E2),
			  edge_reroot( edge{ source => ModAnchor,
					     target => InciseMark,
					     label => void,
					     type => lexical,
					     deriv => Derivs2,
					     id => EId2
					   },
				       Target
				     )
			))
		;
		  edge_reroot( edge{ source => ModAnchor,
				     target => Target,
				     label => Label,
				     type => Type,
				     deriv => Derivs,
				     id => EId1
				   },
			       Mod
			     )
		)
	      ))
	.
	
/*
edge_transform :-
	E1::edge{ source => V::node{},
		  target => Prep::node{ cat => prep },
		  type => subst,
		  label => preparg,
		  id => EId1,
		  deriv => Derivs1
		},
	E2::edge{ source => Prep,
		  target => Head::node{},
		  type => subst,
		  id => EId2,
		  deriv => Derivs2
		},
	erase(E1),
	erase(E2),
	record( edge{ id => EId1,
		      source => V,
		      target => Head,
		      type => subst,
		      label => preparg,
		      deriv => Derivs1
		    }
	      ),
	record( edge{ id => EId2,
			   source => Head,
			   target => Prep,
			   type => subst,
			   label => prep_intro,
			   deriv => Derivs2
			 }
	      )
	.
*/

%% some edge transformation are relation to CONLL conversion

%% CONLL: subject are attached to main verb (including modal verb)
edge_transform :-
	opt(conll),
	E1::edge{  label => 'V',
		   type => adj,
		   source => S,
		   target => T,
		   id => EId,
		   deriv => Derivs
		},
	erase(E1),
	record( conll_modal(S,T) ),
	edge_reroot( edge{ source => T,
			   type => subst,
			   label => object,
			   target => S,
			   deriv => Derivs,
			   id => EId
			 },
		     S
		   ).

%% CONLL: Monsieur is head of syntagm, but dependant in FRMG => we reroot !
edge_transform :-
	opt(conll),
	E1::edge{  label => 'Monsieur',
		   type => lexical,
		   source => S,
		   target => T,
		   id => EId,
		   deriv => Derivs
		},
	every((
	       E2::edge{  label => L2,
			  type => T2,
			  source => S2,
			  target => S,
			  id => EId2,
			  deriv => Derivs2
		       },
	       erase(E2),
	       edge_reroot(edge{ source => S2,
				 target => T,
				 label => L2,
				 type => T2,
				 id => EId2,
				 deriv => Derivs2
			       },
			   T)
	      )),
	erase(E1),
	edge_reroot( edge{ source => T,
			   type => lexical,
			   label => 'Monsieur',
			   target => S,
			   deriv => Derivs,
			   id => EId
			 },
		     S
		   ).


edge_reroot :-
	E::edge{ source => S::node{},
		 target => Start::node{ cat => start },
		 label => start,
		 type => subst
	       },
	node!empty(S),
	( chain( S >> subst >> SAnchor::node{ cat => SAnchorCat, cluster => cluster{ left => Left, right => Right }} ),
	  (Right > Left xor \+ edge{ source => SAnchor } ),
	  \+ SAnchorCat = start,
	  \+ SAnchorCat = end,
	  \+ ( chain( S >> subst >> node{ cat => _SAnchorCat, cluster => cluster{ left => _Left } }),
	       _Left < Left,
	       \+ _SAnchorCat = start,
	       \+ _SAnchorCat = end
	     )
	->
	  Mode = subst,
	  true
	; chain( S >> lexical >> SAnchor ),
	  Right > Left,
	  \+ ( chain( S >> lexical >> node{ cluster => cluster{ left => _Left } }),
	       _Left < Left
	     )
	->
	  Mode = lexical,
	  true
 	;
 	  fail
	),
	format('reroot at ~w anchor=~w\n',[E,SAnchor]),
	erase(E),
	erase(S),
	erase(Start),
	every(( E1::edge{ source => S,
			  target => Target::node{ cat => TargetCat},
			  label => Label,
			  type => Type,
			  deriv => Derivs,
			  id => EId1
			},
		erase(E1),
		(
		 Target = SAnchor ->
		 true
		;
		 domain(TargetCat,[start,end]) ->
		 erase(Target)
		;
		 edge_reroot( edge{ source => SAnchor,
				    target => Target,
				    label => Label,
				    type => Type,
				    deriv => Derivs,
				    id => EId1
				  },
			      S
			    )
		)
	      )),
	every(( E2::edge{ source => K,
			  target => S,
			  label => Label,
			  type => Type,
			  deriv => Derivs,
			  id => EId2
			},
		erase(E2),
		edge_reroot( edge{ source => K,
				   target => SAnchor,
				   label => Label,
				   type => Type,
				   deriv => Derivs,
				   id => EId2
				  },
			     K
			   )
	      ))
	.

edge_transform :-
    E::edge{ id => EId,
	     source => S::node{ tree => STree, id => SNId },
	     label => Label,
	     target => T::node{},
	     type => Kind::edge_kind[subst,lexical],
	     deriv => Derivs,
	     secondary => [edge{ source => Secondary::node{}, label => SecondLabel }]
	   },
    \+ domain(SecondLabel,[ctrsubj]),
    erase(E),
    edge_reroot( edge{ id => EId,
		       source => Secondary,
		       target => T,
		       label => SecondLabel,
		       type => Kind,
		       deriv => Derivs,
		       secondary => []
		     },
		 S
	       )
	.

