#!/usr/bin/perl

use strict;
use File::Basename;

my $success = 0;
my $fail = 0;

my $file = "";

if (@ARGV) {
  $file = $ARGV[0];
  $file = basename($file,'.log');
}

while(<>) {
  $success++, next if (/^ok/);
  $fail++, next if (/^fail/);
}

printf "%-15s Tried: %3i Success: %3.2f%%\n", $file, $success+$fail,100*$success / ($fail + $success);

