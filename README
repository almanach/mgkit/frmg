	frmg

What is frmg
============

	A French grammar generated from a MetaGrammar (MG) and compiled
	with DyALog.

	It is based on a previous MetaGrammar (frenchmg), itself based
	on a previous version developped by Lionel Cl�ment.

	A new simplified notation has been used to simplify the writing
	of the MetaGrammar, with still conversion to an XML exchange
	format.

	The MetaGrammar exploits several new functionalities allowed
	by mgcomp (MG compiler) and DyALog to be more compact. We can
	cite MG classes with namespace (to use several times a same
	class but with different namespaces in a terminal class), free
	node ordering when possible (interleaving), alternatives,
	kleene star nodes, nodes sequences, conditional nodes, ...

	frmg is also part of the Linguistic Processing Chain
	developped by INRIA project team ALPAGE. More information on
	this page:
	http://alpage.inria.fr/alpc.en.html

Contents
========

	INSTALL		instructions for installation
	LICENSE		license and copyright notice
	README		this file

	frgram.smg	the source meta grammar in simplified format
	frgram.tag.xml  (derived file) the resulting TAG grammar (XML format)
	frgram.tag      (derived file) the resulting TAG grammar (DyALog format)
	addons.tag	addons to the grammar, outside the metagrammar

	frmg_lexer	a small segmenter tool (need access to a lexed lexicon)

	error_analyzer.pl a script to compute error rates for words from parse logs

	extract.xsl	to extract part of the grammar (XML format)
	tagml2html.xsl	to convert XML TAG to HTML TAG
	style.css	Style Sheet for HTML TAG

	easyforest.pl	to convert xmldep to XML Easy format
	easy2html.xsl	to convert XML Easy format to HTML Easy format

	sentences.txt   approx. 400 sentences (from Eurotra) for test
	sentences2.txt  sentences tried in web interface
	tsnlp.txt	approx. 1660 sentences for test
	sample_udag	a DAG example

	register_parserd.conf	parsers to be registered within parserd
	frmg_parser	(generated file)

	t/*.t		tests


Installation
============

	See the INSTALL file for the compilation and installation procedure.


Documentation
=============

	To run a sentence (with a lexicon) :

echo "il mange une pomme." | ./frmg_lexer | ./frmg_parser - [-forest] [-robust]

	where:
	-forest : to output the shared derivation forest
	-robust : to do partial parsing

        To run a sentence (without a lexicon) :

echo "il:cln mange:v une:det pomme:nc ." | ./frmg_lexer | ./frmg_parser - [-forest] [-robust]


	For more information, type:

perldoc frmg_lexer
frmg_parser -h
frmg_parser -h

For more documentation, please check Alpage Linguistic Processing
Chain documentation:
<http://alpage.inria.fr/docs/alpchain-doc.pdf>


Availability
============

Get the latest release:
<https://gforge.inria.fr/frs/?group_id=473>

The latest version can also be retrieved from the Subversion
repository on Inria Gforge typing the following command:

svn checkout svn://scm.gforge.inria.fr/svn/mgkit/frmg/trunk frmg


Copyright and Licence
=====================

	INRIA 2004-2010
	GNU Lesser Public License (LGPL)

Author
======

	Send your bug reports and suggestions by E-mail to:
        Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

