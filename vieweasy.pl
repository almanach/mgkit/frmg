#!/usr/bin/perl

## View easy xml in HTML Browser

use 5.006;
use strict;
use warnings;
use Carp;

use IPC::Run qw(start finish run pump);

use AppConfig qw/:argcount :expand/;

use File::Temp qw/tempfile/;
use File::Basename;

my $package= "$ENV{HOME}/Grammars/frmg";
my $xsl = $package."/easy2html.xsl";

my $config = AppConfig->new(
			    'refdir=f' => { DEFAULT => "$ENV{HOME}/.build/CorpusGuide/Resultats" },
			    'compare!' => { DEFAULT => 0 }
			   );

$config->args();

if ($config->compare) {
  foreach my $file (@ARGV) {
    easyhtml_ref_handler($file);
  }
}

easyhtml_handler();

sub easyhtml_handler {
  my ($fh,$filename) = tempfile( DIR => '/tmp/',
				 SUFFIX => '.html',
##				 UNLINK => 0,
			       );
  open(HTML,"| /usr/bin/xsltproc -o $filename $xsl - && dillo -f -l $filename 2> /dev/null") 
    || die "can't run xsltproc";
  print HTML <>;
##  my $h = run ['/usr/bin/dillo','-f','-l',$filename];
}

sub easyhtml_ref_handler {
  my $file = shift;
  print "TRY $file\n";
  my $base = basename($file,'.xml');
  print "BASE $base\n";
  my $pat = $config->refdir()."/".$base."*.xml";
  print "PAT $pat\n";
  my @refs = glob($pat);
  print "FOUND @refs\n";
  return unless @refs;
  $file = $refs[0];
  my ($fh,$filename) = tempfile( DIR => '/tmp/',
				 SUFFIX => '.html',
##				 UNLINK => 0,
			       );
  close $fh;
  if (run ['/usr/bin/xsltproc','-o',$filename,$xsl,$file]) {
    my $h = start ['/usr/bin/dillo','-f','-l',$filename];
  }
}

