/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2021 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  tag_gneric.pl -- Generic Stuff for TAGs
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-require('utils.pl').

:-scanner(check_lexical).
:-skipper(skip_lexical).
	
:-parse_mode(token).

:-features( tag_tree, [ family, name, tree ] ).
:-features( tag_anchor, [name, coanchors, equations ] ).

:-features( lemma, [lex,lemma,cat,top,anchor,truelex,lemmaid] ).

:-rec_prolog normalized_tag_lemma/5.
:-std_prolog anchor/7.
:-extensional tag_lexicon/4.

:-extensional tag_hypertag/2.

:-light_tabular
%	verbose!lexical/5,
	verbose!struct/2,
%	verbose!anchor/7,
%	verbose!coanchor/5,
	verbose!adj/0,
	verbose!epsilon/4,
	verbose!secondary/4
	.

:-extensional autoload_check_hypertag/3.

:-extensional blocking_anchors/2.

:-std_prolog anchor_hypertag/2.
:-std_prolog check_extra_constraints/6.

:-extensional lctag_table/1.

:-extensional tree_constraint/3.

:-require('format.pl').

:-op(  700, xfx, [?=]). % for default value
:-xcompiler(( X ?= V :- ( \+ var(X) xor X = V))). %% Setting a default value

%% return the finite set of all positions
:-extensional '$allpos'/1.

%% 'D'(Cat,CatPos) return all possible positions for Cat
:-extensional 'D'/2.

%% 'T'(Id,Form) return the content Form of token Id
:-extensional 'T'/2.

%% 'S'(SId) return the id of the sentence
:-extensional 'S'/1.

%% 'POSTAG'(Id,Pos) return POS info for token Id
:-extensional 'POSTAG'/2.

%% 'SUPERTAG'(Id,STag) return SuperTag info for token Id
:-extensional 'SUPERTAG'/3.

%% 'DEP'(Id,Label,Delta) return dependency info for token Id
:-extensional 'DEP'/3.

:-extensional 'rC'/2.

%% corpus(File) return the name of currently processed corpus (when available)
:-extensional corpus/1.

%% Finding potential missing coma
:-light_tabular potential_start_coma/1.
:-mode(potential_start_coma/1,+(+)).

%% generalized options
:-xcompiler
xargv(L) :-
    (argv(L) ; recorded(xoptions(L)))
.

%% For guiding
:-extensional guide/0.		% guide
:-extensional guide!node/5.	% guide!node(Left,Right,Form,Cat,Tree)
:-extensional guide!op/5.	% guide!op(Left,Right,XCat,Tree,Caller)
:-extensional guide!supertag/2.	% guide!supertag(TId,Supertag)
:-extensional guide!supertag/1.	% guide!supertag(Supertag)
:-extensional guide!supertag_max/2. % guide!supertag_max(Supertag,TId)

%% to be able to use guiding info
:-xcompiler
guide!test_node(Left,Right,Form,Cat,Tree) :-
	( guide!node(Left,_,_,_,_) ->
	  guide!node(Left,Right,Form,Cat,Tree)
	;
	  true
	)
	.

:-xcompiler
guide!test_op(Left,Right,XCat,Tree,Caller) :-
	( guide!op(Left,_,_,_,_) ->
	      guide!op(Left,Right,XCat,Tree,Caller)
	 ;
	  true
	)
	.

:-mode(try_alternate_trees/1,+(-)).
:-mode(register_alternate_range/1,+(+)).

%:-std_prolog register_rightmost_parsed_token/1.

:-xcompiler
safe_register_rightmost_parsed_token(TId) :-
    (TId=[XTId|_] xor TId=XTId),
    register_rightmost_parsed_token(XTId),
%%    format('registered rightmost parsed token ~w\n',[XTId]),
    true
.

register_rightmost_parsed_token(TId) :-
    (recorded(rightmost_parsed_token(_TId)) ->
	 (TId > _TId ->
	      erase(rightmost_parsed_token(_)),
	      record(rightmost_parsed_token(TId))
	  ;
	  true
	 )
     ;
     record(rightmost_parsed_token(TId))
    )
.

/*
%% to completely deactivate guiding:
:-xcompiler
guide!test_node(Left,Right,Form,Cat,Tree) :- true.

:-xcompiler
guide!test_op(Left,Right,XCat,Tree,Caller) :- true.
*/

%% authorized ellipsis
:-extensional authorized_ellipsis/1.

%% to block the use of a tree
:-extensional tree_block/1.

anchor(Anchor::tag_anchor{ name => HyperTag,
			   coanchors => VarCoanchors,
			   equations => VarEquations
			 },
       Token,
       Cat,
       Left,
       Right,
       Top,
       Addr : [Lemma,TrueLex,LemmaId]
      ) :-
%	format('check anchor ~w ~w ~w cat=~w token=~w\n',[Left,Right,Anchor,Cat,Token]),
	( %fail,
	  Left = gen_pos(L1,L2),
	  number(L1), number(L2), L2 < L1,
	  Right = gen_pos(R1,R2) ->
%	  format('check anchor ~w ~w ~w\n',[Left,Right,Anchor]),
	  ( % ellipis
	    L1=R1,
	    authorized_ellipsis(Cat),
	    anchor(Anchor,_Token,Cat,L2,R2,_Top,Addr : [Lemma,TrueLex,LemmaId]),
	    functor(_Top,F,N),
	    functor(Top,F,N),
	    Token=ellipsis(_Token),
%	    Token=_Token,
%	    format('ellipsis ~w ~w ~w\n',[Left,Right,Token]),
	    true
	  ; authorized_ellipsis(Cat),
	    'C'(L1,lemma{ lex => ',', cat => ponctw, truelex => TrueLex },R1),
	    anchor(Anchor,_Token,Cat,L2,R2,_Top,Addr : [Lemma,_TrueLex,LemmaId]),
	    functor(_Top,F,N),
	    functor(Top,F,N),
	    Token=ellipsis(_Token)
	  ; % no ellipsis
	    anchor(_Anchor,_Token,Cat,L2,R2,_Top,_),
	    anchor(Anchor,Token,Cat,L1,R1,Top,Addr  : [Lemma,TrueLex,LemmaId])
	  )
	;
	  %%	Top = _Top,
	  %%	format( 'LOOKING ANCHOR left=~w ht=~w top=~w\n',[Left,HyperTag,Top] ),
	  recorded(
		   'C'( Left,
			Info::lemma{ lex=> _Token,
				     truelex => _TLex,
				     cat => Cat,
				     top => _Top,
				     lemma => Lemma,
				     lemmaid => LemmaId,
				     truelex => TrueLex,
				     anchor => tag_anchor{ name => HyperTag,
							   coanchors => Coanchors,
							   equations => VarEquations
							 }
				   },
			Right
		      ),
		   Addr
		  ),
	  
	  guide!test_node(Left,Right,_Token,Cat,_),


	  
%%	  format('anchor token=~w _token=~w left=~w\n',[Token,_Token,Left]),
	  ( Token = _Token xor Token = Lemma ),
	  (   cat_normalize(Cat,_Top),
	      %%	    format('Normalize ~w => ~w\n',[Cat,_Top]),
	      true
	  xor true),
	  Top = _Top,
	  %%	anchor_hypertag(HyperTag,Left),
	  anchor_hypertag(HyperTag,_Token),
	  %%	anchor_hypertag(HyperTag,LemmaId),
	  %%	(   var(Lemma) xor check_all_coanchors(VarCoanchors,Coanchors)),
%	  format( 'ANCHOR ~w ~w ~w\n',[Left,Right,Info] ),
	  ( check_extra_constraints(Cat,Top,HyperTag,Coanchors,Left,Right) xor fail ),
	  true
	)
	.

%% authorized ellipsis
:-extensional coanchor_authorized_ellipsis/1.

:-std_prolog coanchor/5.

coanchor(Token,Cat,Left,Right,Top) :-
%%	Top = _Top,
	(  %fail,
	   Left = gen_pos(L1,L2), number(L1), number(L2), L2 < L1,
	   Right = gen_pos(R1,R2) ->

	   ( %% ellipsis
	     L1=R1,
	     coanchor_authorized_ellipsis(Cat),
	     coanchor(_Token,Cat,L2,R2,_Top),
	     functor(_Top,F,N),
	     functor(Top,F,N),
	     Token=ellipsis(_Token)
	   ;
	     %% no ellipsis
	     coanchor(_Token,Cat,L2,R2,_Top),
	     coanchor(Token,Cat,L1,R1,Top)
	   )
	;
	   'xC'(Left,
	       Info::lemma{ lex => Token,
			    lemma => Lemma,
			    cat => Cat,
			    top => _Top },
	       Right
	      ),
	   (   cat_normalize(Cat,_Top) xor true),
	   guide!test_node(Left,Right,Token,Cat,lexical),
	   Top = _Top,
	   %%	format( 'COANCHOR ~w ~w ~w\n',[Left,Right,Info] ),
	   true
	)
	.

:-rec_prolog cat_normalize/2.

:-rec_prolog check_all_coanchors/2.

check_all_coanchors([],[]).

check_all_coanchors([VarLemma|VL],[Lemma|L]) :-
	( Lemma == [] ->
	    (	VarLemma = [] xor true)
	;   var(VarLemma) ->
	    VarLemma = Lemma
	;   VarLemma = check_at_anchor(Left,Right) ->
	    check_coanchor_lemma(Lemma,Left,Right)
	;   
	    fail
	),
	check_all_coanchors(VL,L).

:-std_prolog check_coanchor/3.

check_coanchor(VarLemma,Left,Right) :-
	( var(VarLemma) ->
	    VarLemma = check_at_anchor(Left,Right)
	;   VarLemma == [] ->
	    true
	;   
	    check_coanchor_lemma(VarLemma,Left,Right)
	)
	.

:-std_prolog check_coanchor_in_anchor/2.

check_coanchor_in_anchor(VarLemma,VarLemma).

:-std_prolog check_coanchor_lemma/3.

check_coanchor_lemma(Lemma,Left,Right) :-
	domain(Token,Lemma),
	'xC'(Left,lemma{ lex=>Token },Right)
	.

:-std_prolog skip_lexical/3.
:-finite_set(skipcat,[epsilon,sbound,meta]).
skip_lexical(Left,Token,Right) :-
%	format('Try epsilon ~w\n',[Left]),
	( 'xC'(Left,lemma{ lex => Token, cat => skipcat[] }, Right )
	;
	  fail,			% not yet ready to activate
	  recorded(robust),
	  \+ recorded(is_covered(Left)),
	  ( Left < 4
	  xor XLeft is Left - 3,
	    recorded(is_covered(XLeft))
	  ),
	  'xC'(Left,lemma{ lex => Token, cat => Cat }, Right ),
	  \+ Cat = skipcat[],
	  \+ domain(Cat,[ponctw,poncts])
	),
%	format('Found epsilon ~w ~w\n',[Left,Token]),
	true
	.

:-std_prolog skip_lexical_closure/2.
skip_lexical_closure(Left,Right) :-
	( Right = Left
	;
	skip_lexical(Left,Token,Middle),
	Middle > Left,
	skip_lexical_closure(Middle,Right)
	).

verbose!epsilon(_Lex,Left,Right,[_Lemma,_Lexical,_LemmaId]) :-
	'C'(Left,lemma{lex => _Lex,
		       truelex => _Lexical,
		       cat => skipcat[],
		       lemma => _Lemma,
		       lemmaid => _LemmaId
		      }, Right),
	_Lemma ?= '_Uv',
	true
	.

/*
verbose!epsilon(_Lex,Left,Right,[_Lemma,_Lexical,_LemmaId]) :-
	recorded(robust),
	\+ recorded(is_covered(Left)),
	'C'(Left,lemma{lex => _Lex,
		       truelex => _Lexical,
%		       cat => skipcat[],
		       lemma => _Lemma,
		       lemmaid => _LemmaId
		      }, Right),
	_Lemma ?= '_Uv',
	true
	.
*/

:-std_prolog check_lexical/3.

check_lexical(Left,Token,Right) :-
%%	format('Try check lexical ~w ~w\n',[Left,Token]),
	'xC'(Left,
	    K :: lemma{ lex => Lex,
			truelex => TrueLex,
			lemma => Lemma,
			cat => Cat
		      },
	    Right),
	( var(Token) ->
	  Lex = Token
	; Token = cat(Cat) ->
	  true
	; Token = Lex : Cat ->
	  true
	;  fail,		% not yet ready to activate
	  recorded(robust),
	  \+ recorded(is_covered(Left)),
	  Left > 0,
	  XLeft is Left - 1,
	  recorded(is_covered(XLeft)) ->
	  format('trying added empty coma ~w\n',[Left]),
	  Token = ',',
	  Right = Left
	;
	  (   Lex = Token
	  %%	  xor TrueLex = Token
	  xor \+ var(Lemma),
	      Token = Lemma
	  ),
	  %%	format('Checked lexical ~w ~w => ~w\n',[Left,Token,Right]),
	  %%	'C'(Left,lemma{ cat => Token },Right)
	  true
	),
	guide!test_node(Left,Right,Lex,_,lexical),
	true
	.

%% Dummy predicate used to leave a trace in the forest
verbose!lexical([_Lexical],_Left,_Right,_Cat, [_Lemma,_TrueLex,LemmaId]) :-
	'C'(_Left,
	    K:: lemma{ lex => _Lex,
		       truelex => _TrueLex ,
		       lemma => _Lemma,
		       lemma => _LemmaId,
		       cat => __Cat
		     }, _Right),
%	format('verbose2 lexical lex=~w right=~w K=~w\n',[_Lexical,_Left,K]),
	( _Lex == _Lexical
	%%	xor _TrueLex == _Lexical
	xor _Lemma == _Lexical ),
	_Lemma ?= '_Uv',
	('SUPERTAG'(_TrueLex,_,MD5) ->
	     ( MD5=lexical
	      ;
	      %term_range(_TrueLex,1000,TId),
	      '$answers'(try_alternate_trees(TId)),
	      TId >= _TrueLex,
	      true
	     ),
%	     register_rightmost_parsed_token(_TrueLex),
	     true
	 ;
	 true
	),
	safe_register_rightmost_parsed_token(_TrueLex)
	.

verbose!lexical([_Lex : _Cat ],_Left,_Right,_Cat, [_Lemma,_TrueLex,LemmaId]) :-
	'C'(_Left,
	    K:: lemma{ lex => _Lex,
		       truelex => _TrueLex ,
		       lemma => _Lemma,
		       lemma => _LemmaId,
		       cat => _Cat
		     }, _Right),
	('SUPERTAG'(_TrueLex,__Cat,MD5) ->
	     ( MD5=lexical
	      ;
	      %term_range(_TrueLex,1000,TId),
	      '$answers'(try_alternate_trees(TId)),
	      TId >= _TrueLex,
	      true
	     ),
%	     register_rightmost_parsed_token(_TrueLex),
	     true
	 ;
	 true
	),
	safe_register_rightmost_parsed_token(_TrueLex)
	.

/*
verbose!lexical([_Lexical :: ','],Left,_Left,_Cat,_Info) :-
	_Info = [_Lexical,_Lexical,_Lexical],
	recorded(robust),
	\+ recorded(is_covered(Left)),
	Left > 0,
	XLeft is Left - 1,
	recorded(is_covered(XLeft)),
	format('added empty coma ~w lexical=~w cat=~w info=~w\n',[Left,_Lexical,_Cat,_Info])
	.
*/

/*
verbose!lexical([L::(_;_)],_Left,_Right,_Cat, [_Lemma,_TrueLex]) :-
%%	format('Check verbose lexical ~w\n',[L]),
	check_verbose_lexical(L,_Left,_Right,_Cat,[_Lemma,_TrueLex]).
*/

:-std_prolog check_verbose_lexical/5.

check_verbose_lexical(L,_Left,_Right,_Cat,[_Lemma,_TrueLex]) :-
	( L = (A1;A2) ->
	  ( check_verbose_lexical(A1,_Left,_Right,_Cat,[_Lemma,_TrueLex])
	  ;  check_verbose_lexical(A2,_Left,_Right,_Cat,[_Lemma,_TrueLex])
	  )
	; L = [_Lexical],
	  'C'(_Left,lemma{ lex => _Lex, truelex => _TrueLex , lemma => _Lemma}, _Right),
	  ( _Lex == _Lexical
	  %%	  xor _TrueLex == _Lexical
	  xor _Lemma == _Lexical ),
	  _Lemma ?= '_Uv'
	)
	.

%% Dummy predicate used to leave a trace in the forest
verbose!struct(_Tree_Name,_HyperTag) :-
%	format('Recognized ~w\n',[_Tree_Name]),
	true
	.

%% Dummy predicate used to leave a trace in the forest
verbose!anchor(_Anchor,_Left,_Right,_Tree_Name,_Top, _Addr : [_Lemma,_Lex,_LemmaId], tag_anchor{ name => HT }) :-
%% 	format('VERBOSE ANCHOR anchor=~w left=~w right=~w lemma=~w lex=~w tree=~w\n\tht=~w\n\ttop=~w\n',
%% 	       [_Anchor,_Left,_Right,_Lemma,_Lex,_Tree_Name,HT,_Top]),
	( %fail,
	  _Left = gen_pos(L1,L2), number(L1), number(L2), L2 < L1,
	  _Right = gen_pos(R1,R2) ->
%	  format('verbose anchor left=~w right=~w anchor=~w tree=~w top=~w addr=~w ht=~w\n',
%		 [_Left,_Right,_Anchor,_Tree_Name,_Top,_Addr,HT]),
	  /*
	  ( L1=R1 ->
	    recorded('C'(L2,lemma{ top => __Top,
				   cat => _Cat,
				   truelex => _Lex,
				   lex => __Anchor,
				   lemma => _Lemma,
				   lemmaid => _LemmaId,
				   anchor => tag_anchor{ name => HT }
				 }, R2),
		     _Addr)
	  ;
	    recorded('C'(L1,lemma{ top => __Top,
				   cat => _Cat,
				   truelex => _Lex,
				   lex => __Anchor,
				   lemma => _Lemma,
				   lemmaid => _LemmaId,
				   anchor => tag_anchor{ name => HT }
				 }, R1),
		     _Addr)
	  ),
	  (_Anchor = __Anchor xor _Anchor = _Lemma ),
	  (   cat_normalize(_Cat,__Top) xor true ),
	  _Top = __Top,
	  _Lemma ?= '_Uv'
*/
	  true
	;
	  recorded('C'(_Left,lemma{ top => __Top,
				    cat => _Cat,
				    truelex => _Lex,
				    lex => __Anchor,
				    lemma => _Lemma,
				    lemmaid => _LemmaId,
				    anchor => tag_anchor{ name => HT }
				  }, _Right),
		   _Addr),
	  (_Anchor = __Anchor xor _Anchor = _Lemma ),
	  (   cat_normalize(_Cat,__Top) xor true ),
	  ( blocking_anchors(_Tree_Name,BlockingCats) ->
	    \+ ( domain(_BlockingCat,BlockingCats),
		 'C'(_Left,
		     lemma{ cat => _BlockingCat },
		     _Right
		    )
	       )
	  ;
	    true
	  ),
	  _Top = __Top,
	  _Lemma ?= '_Uv',
	  ('SUPERTAG'(_Lex,__Cat,MD5),
	   MD5 \== 'lexical',
	   %	   __Cat = _Cat
	   true
	   ->
	       
	       ( __Cat = _Cat,
		 (_Tree_Name == MD5 xor recorded(tree2md5(_Tree_Name,MD5)))
		;
		%term_range(_Lex,1000,TId),
		'$answers'(try_alternate_trees(TId)),
		TId >= _Lex,
		'$answers'(register_alternate_range(Range)),
		%		  format('anchor try alternate lex=~w tid=~w range=~w\n',[_Lex,TId,Range]),
		_Lex >= TId-Range,
		true
	       ),
%	       register_rightmost_parsed_token(_Lex),
%	       format('checked md5 for ~w\n',[_Tree_Name]),
	       true
	   ;
	   true
	  ),
	  safe_register_rightmost_parsed_token(_Lex),
	  true
	)
	.

%% Dummy predicate used to leave a trace in the forest
verbose!coanchor(_Lex,_Left,_Right,_Top,[_Lemma,_TrueLex,_LemmaId]) :-
	( %fail,
	  _Left = gen_pos(L1,L2), number(L1), number(L2), L2 < L1,
	  _Right = gen_pos(R1,R2) ->
	  (L1=R1 -> _L=L2, _R=R2 ; _L = L1, _R=R1)
	;
	  _L=_Left,
	  _R=_Right
	),
	( _Lex  = ellipsis(_XLex) xor _Lex = _XLex),
	'C'(_L,
	    lemma{ top => __Top,
		   cat => _Cat,
		   lex => _XLex,
		   truelex => _TrueLex,
		   lemma => _Lemma,
		   lemmaid => _LemmaId
		 },
	    _R),
	(   cat_normalize(_Cat,__Top) xor true ),
	_Top = __Top,
	%%	format('VERBOSE COANCHOR ~w ~w ~w ~w\n',[_Left,_Lemma,_TrueLex,_Right]),
	_Lemma ?= '_Uv',
	('SUPERTAG'(_TrueLex,__Cat,MD5) ->
	     ( MD5=lexical,
	       _Cat = __Cat
	      ;
%	      term_range(_TrueLex,1000,TId),
	      '$answers'(try_alternate_trees(TId)),
	      TId >= _TrueLex,
	      '$answers'(register_alternate_range(Range)),
%	      format('coanchor try alternate lex=~w tid=~w range=~w\n',[_TrueLex,TId,Range]),
	      _TrueLex >= TId - Range,
%	      format('verbose!coanchor alternate TId=~w TrueLex=~w\n',[TId,_TrueLex]),
	      true
	     ),
%	     register_rightmost_parsed_token(_TrueLex),
	     true
	 ;
	 true
	),
	safe_register_rightmost_parsed_token(_TrueLex),
	true
	.
	
%% Dummy predicate used to leave a trace in the forest
verbose!adj.

%% Dummy predicate used to leave a trace in the forest about secondary edges
verbose!secondary(Label,Source2,Label2,Axis).

%:-std_prolog secondary/3.

:-xcompiler
secondary(Label,Source2,Label2,Axis) :-
%    format('handling secondary ~w ~w ~w\n',[Label,Source2,Label2]),
    (atom(Label),
     atom(Source2),
     atom(Label2),
     Axis ?= local,
     %	 format('\tok\n',[]),
     (true xor format('secondary\n',[])),
     '$tagop'(Label,verbose!secondary(Label,Source2,Label2,Axis)),
     %	 format('\tdone\n',[]),
     true
     xor
     true
    )
.

:-rec_prolog normalize_coanchors/1.

normalize_coanchors([]).
normalize_coanchors([VarCoanchors|VC]) :-
	(VarCoanchors = [] xor true ),
	normalize_coanchors(VC)
	.

:-xcompiler
xC(Left,
   L::lemma{},
   Right) :-
    ('C'(Left,L,Right) ; recorded(reserved('C'(Left,L,Right))))
	.

:-std_prolog tag_family_load/7.

tag_family_load(HyperTag,Cat,Top,Token,Name,Left,Right) :-
%%	format('TAG FAMILY LOADER cat=~w left=~w ht=~w name=~w\n',[Cat,Left,HyperTag,Name]),
	(   %%format('** Hypertag = ~w\n',[Hypertag]),
	    'D'(Cat,DLeft),
	    domain(Left,DLeft),
	    'xC'(Left,
		lemma{ cat => Cat,
		       top => _Top,
		       lex => Lex,
		       lemma => Lemma,
		       anchor => tag_anchor{ name => _HyperTag }
		     },
		Right),
	    (	cat_normalize(Cat,_Top) xor true),
	    %% Next line to handle support verbs (should find a more elegant way)
	    %% The trouble is that support verbs inherits their frame from arg1
	    %% but this inheritance is done after the autoloading phase (via clean_sentences)
	    %% April 2011: not sure it works (no ncpredobj in hypertags !)
	    \+ \+ ( 
		    Top = _Top,
		    (HyperTag = _HyperTag
		    xor autoload_check_hypertag(Cat,HyperTag,_HyperTag)),
		    (	  Lex = Token
		    xor (\+ var(Lemma), Lemma = Token)
		    )
		  ),

	    ( blocking_anchors(Name,BlockingCats) ->
	      \+ ( domain(_BlockingCat,BlockingCats),
		   'xC'(Left,lemma{ cat => _BlockingCat },Right)
		 )
	    ;
	      true
	    ),

	    assert_anchor_points(Cat,HyperTag,Top,Name,Token),
%	    format('--> To be loaded cat=~w name=~w ~w ~w ~w\n',[Cat,Name,HyperTag,Left,Right]),
	    true
	)
	xor fail
	.


%:-std_prolog assert_anchor_points/5.

:-xcompiler
assert_anchor_points(Cat,HyperTag,Top,Name,Token) :-
	( recorded( XPoint::require_anchor(Name) )
	xor
	  record( XPoint ),
	  every((
		 'D'(Cat,DLeft),
		 domain(Left,DLeft),
		 \+  recorded( Point::anchor_point(Name,Left,Cat) ),
		 'xC'(Left,
		      lemma{ cat => Cat,
			     top => Top,
			     lex => Lex,
			     truelex => TLex,
			     lemma => Lemma,
			     anchor => tag_anchor{ name => HyperTag }
			 },
		      _
		    ),
		 ( Lex = Token xor (\+ var(Lemma), Lemma = Token)),
		 record( Point ),
%%		 format('anchor point ~w ~w\n',[Name,Left]),
		 (fail,
		  recorded('SUPERTAG'(TLex,_Cat,MD5)) ->
		      _Cat=Cat,
		      (Name==MD5 xor recorded(tree2md5(Name,MD5)))
		  ;
		  true
		 ),
		 true
		)
	       )
	)
	.
	
%:-std_prolog tag_check_coanchor/6.

:-xcompiler
tag_check_coanchor(Cat,Top,Token,Left,Right,Name) :-
%	format('search coanchor ~w at ~w top=~w token=~w tree=~w\n',[Cat,Left,Top,Token,Name]),
	((   'D'(Cat,DLeft),
	    domain(Left,DLeft),
	    'xC'(Left,
		X::lemma{ cat => Cat, lex => Lex, lemma => Lemma, lemmaid => LemmaId, top => _Top },
		Right),
	    (	cat_normalize(Cat,_Top) xor true),
	    \+ \+ ( Top = _Top,
		      (	  Lex = Token
		      xor (\+ var(LemmaId), LemmaId = Token)
		      xor (\+ var(Lemma), Lemma = Token)
		      )),
	    /* still unsafe
	    every((
		   domain(_Left,DLeft),
		   Left =< _Left, % Left is the smallest possible place for a coanchor
		   \+ recorded( Point::coanchor_point(Name,_Left) ),
		   record( Point )
		  )),
	    */
	    true
	)  xor fail),
%	format('checked coanchor ~w at ~w\n',[X,Left]),
	true
	.

%:-std_prolog tag_check_foot/6.

:-xcompiler
tag_check_foot(Cat,Top,Left,Right,Name,Mode) :-
%%	format('search foot ~w at left=~w right=~w\n',[Cat,Left,Right]),
	( 'D'(Cat,DLeft),
	  domain(Left,DLeft),
	  'xC'(Left,X::lemma{ cat => Cat },Right),
	  every(( Mode == left,
		  domain(_Left,DLeft),
		  Left =< _Left, %% Left is the smallest possible place for a foot
		  \+ recorded( Point::foot_point(Name,_Left) ),
		  record( Point )
		))
	) xor fail
	%% format('checked foot ~w at ~w\n',[X,Left]),
	%% true
	.

%:-std_prolog tag_check_subst/5.

:-xcompiler
tag_check_subst(Cat,InCats,Left,Right,Name) :-
	( domain(InCat,InCats),
	  'D'(InCat,DLeft),
	  domain(Left,DLeft),
	  'xC'(Left,lemma{ cat => InCat },Right),
	  /* still unsafe
	  every(( domain(_InCat,InCats),
		  'D'(_InCat,_DLeft),
		  domain(_Left,_DLeft),
		  \+ recorded( Point::subst_point(Name,_Left) ),
		  record( Point )
		))
	  */
	  true
	) xor fail
	.

%% defined in main
:-std_prolog tag_autoload_adj/7.
	  
:-extensional ok_tree/2.
:-extensional lctag/4.
%%:-light_tabular tag_filter/1.

:-std_prolog tag_filter_init/0.

:-extensional lctag_ok/1.
:-extensional lctag_map/3.
:-extensional lctag_map_reverse/3.
:-extensional ok_cat/3.
:-extensional loaded_tree/1.

:-std_prolog load_lctag_data/0.

load_lctag_data :-
    every((
		 %		 format('process lctag table\n',[]),
		 lctag_table(LTable),
		 %		 format('process lctag table2\n',[]),
		 domain(Data,
			[
			    lctag_proba_cat(_,_),
			    lctag_proba_token(_,_),
			    lctag_proba_pos(_,_),
			    lctag_proba_poscat(_,_,_),
			    lctag_proba_tokencat(_,_,_),
			    lctag_proba_rcat(_,_),
			    lctag_proba_cat_rcat(_,_,_),
			    lctag_proba_rtoken(_,_),
			    lctag_proba_cat_caller(_,_,_,_,_),
			    lctag_proba_allow(_),
			    toto
			]),
		 recorded(Data),
%		 format('add table ~w\n',[Data]),
		 local_table!record(LTable,Data)
	     ))
.

:-light_tabular check_lctag_proba_allow/1.
:-mode(check_lctag_proba_allow/1,+(+)).

check_lctag_proba_allow(Tree) :-
    lctag_table(Table),
    (recorded(tree2md5(Tree,MD5)) xor Tree=MD5),
    local_table!recorded(Table,lctag_proba_allow(MD5)),
    true
.

:-light_tabular check_lctag_proba_cat/2.
:-mode(check_lctag_proba_cat/2,+(+,+)).

check_lctag_proba_cat(Cat,Tree) :-
    lctag_table(Table),
    (
	check_lctag_proba_allow(Tree) ->
	true
    ; 	local_table!recorded(Table,lctag_proba_cat(Cat,_)) ->
	    (recorded(tree2md5(Tree,MD5)) xor Tree=MD5),
	    local_table!recorded(Table,lctag_proba_cat(Cat,MD5)),
	    true
     ;
     true
    )
.

:-light_tabular check_lctag_proba_lex/2.
:-mode(check_lctag_proba_lex/2,+(+,+)).

check_lctag_proba_lex(Lex,Tree) :-
    lctag_table(Table),
%    format('check ~w\n',[Lex]),
    (%fail,
	check_lctag_proba_allow(Tree) ->
	true
    ; local_table!recorded(Table,lctag_proba_token(Lex,_)) ->
      (recorded(tree2md5(Tree,MD5)) xor Tree=MD5),
      %	 format('checking lctag ~w ~w ~w\n',[Lex,MD5,Tree]),
	 local_table!recorded(Table,lctag_proba_token(Lex,MD5)),
	 %	 format('ok\n',[]),
	 true
    ;
    true
    )
.

:-light_tabular check_lctag_proba_pos/2.
:-mode(check_lctag_proba_pos/2,+(+,+)).

check_lctag_proba_pos(Pos,Tree) :-
    lctag_table(Table),
    (%fail,
	check_lctag_proba_allow(Tree) ->
	true
    ; local_table!recorded(Table,lctag_proba_pos(Pos,_)) ->
	 (recorded(tree2md5(Tree,MD5)) xor Tree=MD5),
	 %	 format('checking lcat ~w ~w ~w\n',[Cat,MD5,Tree]),
	 local_table!recorded(Table,lctag_proba_pos(Pos,MD5))
     ;
     true
    )
.

:-light_tabular check_lctag_proba_poscat/3.
:-mode(check_lctag_proba_poscat/3,+(+,+,+)).

check_lctag_proba_poscat(Pos,Cat,Tree) :-
    lctag_table(Table),
    (%fail,
     check_lctag_proba_allow(Tree) ->
	true
    ; local_table!recorded(Table,lctag_proba_poscat(Pos,Cat,_)) ->
	 (recorded(tree2md5(Tree,MD5)) xor Tree=MD5),
	 %	 format('checking lcat ~w ~w ~w\n',[Cat,MD5,Tree]),
	 local_table!recorded(Table,lctag_proba_poscat(Pos,Cat,MD5))
     ;
     true
    )
.

:-light_tabular check_lctag_proba_tokencat/3.
:-mode(check_lctag_proba_tokencat/3,+(+,+,+)).

check_lctag_proba_tokencat(Lex,Cat,Tree) :-
    lctag_table(Table),
    (%fail,
     check_lctag_proba_allow(Tree) ->
	true
    ; local_table!recorded(Table,lctag_proba_tokencat(Lex,Cat,_)) ->
	 (recorded(tree2md5(Tree,MD5)) xor Tree=MD5),
	 %	 format('checking lcat ~w ~w ~w\n',[Cat,MD5,Tree]),
	 local_table!recorded(Table,lctag_proba_tokencat(Lex,Cat,MD5))
     ;
     true
    )
.


:-light_tabular check_lctag_proba_rlex/2.
:-mode(check_lctag_proba_rlex/2,+(+,+)).

check_lctag_proba_rlex(Lex,Tree) :-
    lctag_table(Table),
%    format('check ~w\n',[Lex]),
    (%fail,
     check_lctag_proba_allow(Tree) ->
	true
    ; local_table!recorded(Table,lctag_proba_rtoken(Lex,_)) ->
	 (recorded(tree2md5(Tree,MD5)) xor Tree=MD5),
%	 format('checking lctag ~w ~w ~w\n',[Lex,MD5,Tree]),
	 local_table!recorded(Table,lctag_proba_rtoken(Lex,MD5)),
%	 format('ok\n',[]),
	 true
     ;
     true
    )
.

:-light_tabular check_lctag_proba_rcat/2.
:-mode(check_lctag_proba_rcat/2,+(+,+)).

check_lctag_proba_rcat(Cat,Tree) :-
    lctag_table(Table),
%    format('check ~w\n',[Lex]),
    (%fail,
     check_lctag_proba_allow(Tree) ->
	true
    ; local_table!recorded(Table,lctag_proba_rcat(Cat,_)) ->
	 (recorded(tree2md5(Tree,MD5)) xor Tree=MD5),
%	 format('checking lctag ~w ~w ~w\n',[Cat,MD5,Tree]),
	 local_table!recorded(Table,lctag_proba_rcat(Cat,MD5)),
%	 format('ok\n',[]),
	 true
     ;
     true
    )
.

:-light_tabular check_lctag_proba_cat_rcat/3.
:-mode(check_lctag_proba_rcat/3,+(+,+,+)).

check_lctag_proba_cat_rcat(Cat,RCat,Tree) :-
    lctag_table(Table),
%    format('check ~w\n',[Lex]),
    (%fail,
     check_lctag_proba_allow(Tree) ->
	true
    ; local_table!recorded(Table,lctag_proba_cat_rcat(Cat,RCat,_)) ->
	 (recorded(tree2md5(Tree,MD5)) xor Tree=MD5),
%	 format('checking lctag ~w ~w ~w\n',[Cat,MD5,Tree]),
	 local_table!recorded(Table,lctag_proba_cat_rcat(Cat,RCat,MD5)),
%	 format('ok\n',[]),
	 true
     ;
     true
    )
.


%:-std_prolog check_lctag_proba_cat_lex_pos/4.

%:-light_tabular check_lctag_proba_cat_lex_pos/4.
%:-mode(check_lctag_proba_cat_lex_pos/4,+(+,+,+,+)).

:-xcompiler
check_lctag_proba_cat_lex_pos(Cat,Lex,Pos,Tree) :-
    lctag_table(Table),
    (recorded(tree2md5(Tree,MD5)) xor Tree=MD5),
    (check_lctag_proba_allow(Tree) ->
	true
    ; Pos > 2,
     true ->
	 ( (local_table!recorded(Table,lctag_proba_tokencat(Lex,Cat,_)) ->
	       Tried = yes,
	       (local_table!recorded(Table,lctag_proba_tokencat(Lex,Cat,MD5)) -> Keep = yes ; true)
	    ;
	    true
	   ),
	   (%(Tried \== yes xor Keep == yes),
	       Keep \== yes,
	       local_table!recorded(Table,lctag_proba_token(Lex,_)) ->
		   Tried = yes,
		   ( local_table!recorded(Table,lctag_proba_token(Lex,MD5)) -> Keep = yes ; true )
	    ;
	    true
	   ),
	   (
	       Keep \== yes,
	       local_table!recorded(Table,lctag_proba_cat(Cat,_)) ->
		   Tried = yes,
		   (local_table!recorded(Table,lctag_proba_cat(Cat,MD5)) -> Keep = yes ; fail)
	    ;
	    true
	   )
	 )
     ;
     (local_table!recorded(Table,lctag_proba_poscat(Pos,Cat,_)) ->
	   Tried = yes,
	   (local_table!recorded(Table,lctag_proba_poscat(Pos,Cat,MD5)) ->  Keep = yes ; true )
      ;
      true
     ),
     (%(Tried \== yes xor Keep == yes),
	 Keep \== yes,
	 local_table!recorded(Table,lctag_proba_token(Lex,_)) ->
	     Tried = yes,
	     ( local_table!recorded(Table,lctag_proba_token(Lex,MD5)) -> Keep = yes ; true )
      ;
      true
     ),
     (
	 local_table!recorded(Table,lctag_proba_cat(Cat,_)) ->
	     Tried = yes,
	     (local_table!recorded(Table,lctag_proba_cat(Cat,MD5)) -> Keep = yes ; fail)
      ;
      true
     ),
     true
    ),
    (Tried = no xor Keep == yes)
.
    


:-std_prolog check_ok_cat/6.

%:-xcompiler
check_ok_cat(Name,Cat,Mode,Left,Right,Top) :-
	%% Name denote the name of the calling tree
    %	format('Check ok name=~w left=~w cat=~w mode=~w top=~w\n',[Name,Left,Cat,Mode,Top]),
    \+ recorded(debug_block_cat(Left,Cat,Mode)),
	(  var(Left) ->
	  ( lctag_map(Cat,Mode,Tree),
	    ( lctag_ok(Tree)  xor check_ok_tree_aux(Tree,Left))
	  xor fail
	  )
	 ; recorded(strong_ok_cat(Cat,Mode,Left)) ->
	       true
	 ; ok_cat(Cat,Mode,Left) ->
	   %% ok_cat are installed by tag_filter, or by the following case
%%	   control_continuations(Cat,Mode,Left),
	       guide!test_op(Left,_,Cat,_,_),
	       
	       %	(Mode = subst -> _Mode = Mode ; _Mode = adj),
	       %	format('try ok cat left=~w name=~w cat=~w mode=~w\n',[Left,Name,Cat,Mode]),
	       (recorded(caller_at(Left,Name,Cat,Mode)) ->
		    true
		;
		record(caller_at(Left,Name,Cat,Mode)),
		( recorded(lctag_caller_noconstraint(Left,Cat,Mode)) ->
		      true
		 ; number(Name) ->
		       %	       format('caller no constraint left=~w name=~w\n',[Left,Name]),
		       record_without_doublon(lctag_caller_noconstraint(Left,Cat,Mode))
		 ; recorded('N'(Left)) ->
		       %		format('caller no constraint left=~w name=~w\n',[Left,Name]),
		       record_without_doublon(lctag_caller_noconstraint(Left,Cat,Mode))
		 ;
		 every((lctag_table(Table),
			(recorded(tree2md5(Name,MD5)) xor Name=MD5),
			'C'(Left,lemma{ cat => _Cat },_),
			(
			 local_table!recorded(Table,lctag_proba_cat_caller(_Cat,Cat,Mode,MD5,_)) ->
			 (local_table!recorded(Table,lctag_proba_cat_caller(_Cat,Cat,Mode,MD5,_MD5))
			 ; local_table!recorded(Table,lctag_proba_allow(_MD5))
			 ),
			 record_without_doublon(lctag_caller_activate(Left,_MD5))
			 ;
			 %		  format('caller no constraint left=~w name=~w\n',[Left,Name]),
			 record_without_doublon(lctag_caller_noconstraint(Left,Cat,Mode))
			)
		       ))
		)
	       ),

	       record_without_doublon(strong_ok_cat(Cat,Mode,Left)),
	       
	       true
	; lctag_map(Cat,Mode,Tree),
	  lctag_ok(Tree)
	  ->
	   %% Tree has no lctag constraints
	   %% il may be started anywhere
	      record(ok_cat(Cat,Mode,_)),


	      (recorded(caller_at(Left,Name,Cat,Mode)) ->
		   true
	       ;
	       record(caller_at(Left,Name,Cat,Mode)),
	       ( recorded(lctag_caller_noconstraint(Left,Cat,Mode)) ->
		     true
		; number(Name) ->
		      %	       format('caller no constraint left=~w name=~w\n',[Left,Name]),
		      record_without_doublon(lctag_caller_noconstraint(Left,Cat,Mode))
		; recorded('N'(Left)) ->
		      %		format('caller no constraint left=~w name=~w\n',[Left,Name]),
		      record_without_doublon(lctag_caller_noconstraint(Left,Cat,Mode))
		;
		every((lctag_table(Table),
		       (recorded(tree2md5(Name,MD5)) xor Name=MD5),
		       'C'(Left,lemma{ cat => _Cat },_),
		       (
			local_table!recorded(Table,lctag_proba_cat_caller(_Cat,Cat,Mode,MD5,_)) ->
			     local_table!recorded(Table,lctag_proba_cat_caller(_Cat,Cat,Mode,MD5,_MD5)),
			     record_without_doublon(lctag_caller_activate(Left,_MD5))
			;
			%		  format('caller no constraint left=~w name=~w\n',[Left,Name]),
			record_without_doublon(lctag_caller_noconstraint(Left,Cat,Mode))
		       )
		      ))
	       )
	      ),

	      record_without_doublon(strong_ok_cat(Cat,Mode,Left)),
	      
%%	   control_continuations(Cat,Mode,Left),
	   true
	;  %fail,
	   Left = gen_pos(L1,L2), number(L1), number(L2), L2 < L1
	->
	   ( Mode = subst ->
	     check_ok_cat(Name,Cat,Mode,L2,Right,Top)
%	     check_ok_cat(Cat,Mode,L1,Right)
	    ;
	    (check_ok_cat(Name,Cat,Mode,L2,Right,Top) xor check_ok_cat(Name,Cat,Mode,L1,Right,Top)),
	     true
	   ),
	   true
	;
	   fail
	),
	( 
	  recorded(opt(altstats)),
%	  \+ recorded(checked_cat(Left,Name,Cat,Mode,Top)),
	  true  ->
	      (recorded(tree2md5(Name,MD5)) xor Name=MD5),
	      format('+++ cat ~w ~w ~w ~w ~w ~w\n',[MD5,Cat,Mode,Left,Top,Name]),
	      record_without_doublon(checked_cat(Left,Name,Cat,Mode,Top)),
	      true
	 ;
	 true
	),
	%	format('cat selected left=~w cat=~w mode=~w right=~w\n',[Left,Cat,Mode,Right] ),
%	record_without_doublon(checked_cat(Left,Name,Cat,Mode,Top)),
	true
	.

:-std_prolog control_continuations/3.

control_continuations(Cat,Mode,Left) :-
	( recorded( control_cont(Left,Cat,Mode,M) ) ->
	  mutable_inc(M,V),
	  V < 100
	; mutable(M,1),
	  record( control_cont(Left,Cat,Mode,M) )
	)
	.

:-std_prolog check_ok_tree_aux/2.

check_ok_tree_aux(Name,Left) :-
%	format('try check at ~w for ~w cat=~w mode=~w\n',[Left,Name,Cat,Mode]),
	\+ recorded(tree_block(Name)),
	\+ recorded(debug_block_tree(Name,Left)),
	( var(Left) ->
	  ( ok_tree(Name,_Left) xor fail )
	; %fail,
	  Left = gen_pos(L1,L2), number(L1), number(L2), L2 < L1 ->
	  (ok_tree(Name,L1) xor ok_tree(Name,L2)),
	  true
	; recorded(debug_ok_tree(Name,Left)) ->
	      true
%	; recorded(robust) ->
%	      true
	; ok_tree(Name,Left),
%	  format('almost ok left=~w name=~w\n',[Left,Name]),
	  lctag_map_reverse(Name,Cat,Mode),
	  (recorded(lctag_caller_noconstraint(Left,Cat,Mode))
 	   ->
	       true
	   ; (recorded(tree2md5(Name,MD5)) xor Name=MD5),
	     recorded(lctag_caller_activate(Left,MD5)),
	     true
	     ->
		 true
	   ;
	   record_without_doublon(reserved_lctag_caller_activate(Name,Left)),
	   fail
	  ),
%	  format('\tOK\n',[]),
	  true
	),
	(recorded(opt(altstats)) ->
	     (recorded(tree2md5(Name,MD5)) xor Name=MD5),
	     format('tree selected left=~w md5=~w tree=~w\n',[Left,MD5,Name] )
	 ;
	 true
	),
	true
	.

%:-mode(check_ok_tree/2,+(+,+)).
%:-std_prolog check_ok_tree/2.

:-xcompiler
check_ok_tree(Name,Left) :-
    ( check_ok_tree_aux(Name,Left)
     ; \+ var(Left),
       ( recorded(reserved_ok_tree(Name,Left)),
         \+ recorded(ok_tree(Name,Left))
	;
	recorded(reserved_lctag_caller_activate(Name,Left)),
	recorded(ok_tree(Name,Left)),
	true
       ),
       check_ok_tree_ter(Name,Left),
       \+ recorded( exists_full_parse ),
       true
    )
	.

:-mode(check_ok_tree_ter/2,+(+,+)).

check_ok_tree_ter(Name,Left) :-
    '$answers'(register_started_lctag),
    recorded(pos2tid(Left,_TId)),
    '$answers'(register_rightmost_parsed_token(TId)),
    _TId < TId + 5,
    %       format('try ok tree ter left=~w tid=~w name=~w\n',[Left,TId,Name]),
    '$answers'(register_alternate_range(Range)),
    _TId > TId - Range,
    %       format('ok tree ter left=~w tid=~w name=~w\n',[Left,TId,Name]),
    true
.

check_ok_tree_ter(Name,Left) :-
    '$answers'(register_activate_delayed_lctag_callers),
    recorded(reserved_lctag_caller_activate(Name,Left)),
    true
.

    

/*
check_ok_tree(Name,Left) :-
%    fail,
    recorded(reserved_ok_tree(Name,Left)),
    %    '$answers'(try_alternate_trees(_)),
    '$answers'(register_started_lctag),
    '$answers'(register_rightmost_parsed_token(TId)),
    'xC'(Left,lemma{ truelex => TLex},_),
    (TLex = [_TId|_] xor TLex = _TId),
    _TId < TId + 2,
    true
	.
*/

:-extensional
	require_anchor/1,
	anchor_point/3,
	coanchor_point/2,
	foot_point/2,
	subst_point/2,
	added_correction/0
	.

tag_filter_init :-
	%% remove some anchoring points that were found at autoload time
	%% but are no longer valid after clean_sentence
	every(( xargv(Argv),
		\+ domain('-no_lctag',Argv),
		K::anchor_point(Name,Left,Cat),
		\+ 'xC'(Left,lemma{ cat => Cat },_),
		erase( K )
	      ))
	.

register_ok_tree(Left,Name).

tag_filter(Left) :-
	xargv(Argv),
	domain('-no_lctag',Argv),
	recorded('N'(Left)),
	lctag(_,_,Name,_),
	\+ recorded( block_tree(Name,Left) ),
	\+ recorded(OK::ok_tree(Name,Left)),
	record(OK),
	every(( lctag_map_reverse(Name,_Cat,_Mode),
		\+ recorded( OK_CAT::ok_cat(_Cat,_Mode,Left)),
		record(OK_CAT)
	      )),
	true
	.

tag_filter(Left) :-
	xargv(Argv),
	domain('-no_lctag',Argv),
	'xC'(Left,
	    lemma{ lex => Lex,
		   truelex => TrueLex,
		   lemma => Lemma,
		   cat => Cat
		 },
	    Right),
	Right > Left,
	lctag(_,_,Name,_),
	\+ recorded( block_tree(Name,Left) ),
	\+ recorded(OK::ok_tree(Name,Left)),
	record(OK),
	every(( lctag_map_reverse(Name,_Cat,_Mode),
		\+ recorded( OK_CAT::ok_cat(_Cat,_Mode,Left)),
		record(OK_CAT)
	      )),
	true
	.

tag_filter(Left) :-
%%	format('Try LC selected ~w ~w\n',[Left,Name]),
	xargv(Argv),
	\+ domain('-no_lctag',Argv),
	(K::'C'(Left,
	    lemma{ lex => Lex,
		   truelex => TrueLex,
		   lemma => Lemma,
		   cat => Cat,
		   top => Top
		 },
	    Right)
	 ;
	 recorded(reserved(K)),
	 Now = no_reserved
	),
	Right > Left,
	( cat_normalize(Cat,Top) xor true ),
	%% check basic trees first
	( (
		lctag(cat,Cat:Top,Name,Trees),
		(check_lctag_proba_allow(Name) ->
		     true
		; check_lctag_proba_cat(Cat,Name),
		 true
		 ->
		     true
		 ;
%		 Now = no
		 fail
		),
		( check_lctag_proba_allow(Name) ->
		  true
		; (Left > 3 xor check_lctag_proba_poscat(Left,Cat,Name)),
		  (check_lctag_proba_tokencat(Lex,Cat,Name)),
		  true
		 ->
		      true
		 ; Now == no_reserved ->
		       fail
		 ;
		 Now = no_lcproba
		),
		true
%	   Kind = cat
	  ; lctag(scan,Lex,Name,Trees)
%	   Kind = scan
%	  ; Lex \== TrueLex,
%	   lctag(scan,TrueLex,Name,Trees)
%	   Kind = scan
	  ; lctag(scan,Lemma,Name,Trees)
	  ; lctag(scan,Lex : Cat, Name, Trees ), %alternate form for some scans (see incise in addons)
%	    check_lctag_proba_cat(Cat,Name),
%	    check_lctag_proba_tokencat(Lex,Cat,Name),
	    true
%	   Kind = scan
	  ; lctag(pos,Left,Name,Trees)
%	   Kind = pos
	  ; lctag(special,special,Name,Trees),
	   special_lctag(Name,Left),
%	   Kind = special,
	   true
	  ; Cat = skipcat[],
	    %format('after skip at ~w\n',[Left]),
	    %'$answers'(tag_filter(Right)),
	    '$answers'(register_ok_tree(Right,Name)),
	    %ok_tree(Name,Right),
	    Kind = skip
	  ),
	  \+ recorded( block_tree(Name,Left) ),
	  \+ recorded(OK::ok_tree(Name,Left)),
%	    format('potential ok_tree left=~w tree=~w\n',[Left,Name]),
	  guide!test_op(Left,_,_Cat,Name,_),
	  ( Kind == skip
	  xor
	  (  ( Trees = [] ->
	       check_close_anchor_point(Name,Left)
	     ;
%	       format('try first lctag left=~w trees=~w name=~w\n',[Left,Trees,Name]),
	       check_first_lctag_tree(Trees,Left),
%	       format('ok first lctag left=~w trees=~w name=~w\n',[Left,Trees,Name]),
	       check_anchor_point(Name,Left)
	     ),
%%	    check_coanchor_point(Name,Left),
	    check_foot_point(Name,Left),
%%	    check_subst_point(Name,Left),
	    true
	  )
	  xor fail
	  ),
	  ((tree_constraint(Name,TCst,TDist)
			   xor
			   tree2md5(Name,MD5), tree_constraint(MD5,TCst,TDist)) ->
	       _Right is Left+TDist,
	       term_range(Left,_Right,TLeft),
%	       format('check tree constraint md5=~w cst=~w tleft=~w\n',[MD5,TCst,TLeft]),
	       ('C'(TLeft,TCst,_) xor fail),
%	       format('\tfound at ~w\n',[TLeft]),
	       true
	   ;
	   true
	  ),

	  ( %Now = yes,
	      ( check_lctag_proba_allow(Name) 
	      xor 
	      check_lctag_proba_lex(Lex,Name),
		(Left > 3 xor check_lctag_proba_pos(Left,Name))
	      ),
	    /*
	    (Left = 0 xor
		      'rC'(Left,RLeft),
		      'C'(RLeft,lemma{ lex => RLex, cat => RCat },Left),
		      check_lctag_proba_cat_rcat(Cat,RCat,Name)
		      % check_lctag_proba_rcat(RCat,Name)
	    ),
*/
	    true
	   ->
	       (Now = yes ->
		    (recorded(OK) xor
		     record(OK),
%		     format('register ~w\n',[OK]),
		     register_ok_tree(Left,Name)
		    )
		;
		(recorded(OK) xor record_without_doublon(reserved_ok_tree(Name,Left)))
	       )
	   ; Now == no_reserved ->
		 true
	   ;
	   (recorded(OK) xor record_without_doublon(reserved_ok_tree(Name,Left))),
	   true
	  ),
	  every(( lctag_map_reverse(Name,_Cat,_Mode),
		  \+ recorded( OK_CAT::ok_cat(_Cat,_Mode,Left) ),
		  record(OK_CAT)
		)),
%%	  format('ok_tree left=~w kind=~w cat=~w lex=~w tree=~w\n',[Left,Kind,Cat,Lex,Name]),
	  fail
	%%	  format('ok_tree left=~w tree=~w\n',[Left,Name]),
	;
	  %%	tabulated_tag_filter(Name,Left),
	  %%	format('LC selected ~w ~w\n',[Left,Name]),
	  true
	)
	.

tag_filter(Right) :-
%%	format('Try LC selected ~w ~w\n',[Left,Name]),
	argv(Argv),
	\+ domain('-no_lctag',Argv),
	'rC'(Right,Left),
	Right > Left,
	(K:: 'C'(Left,
		 lemma{ lex => Lex,
			%		   truelex => TrueLex,
			lemma => Lemma,
			cat => Cat,
			top => Top
		      },
		 Right)
	  ;
	  recorded(reserved(K)),
	  Now = no
	),
%	( cat_normalize(Cat,Top) xor true ),
	( 
	  ( lctag(postscan,Lex,Name,Trees)
%	  ; Lex \== TrueLex,
%	    lctag(postscan,TrueLex,Name,Trees)
	  ; lctag(postcat,Cat:Top,Name,Trees)
	  ; lctag(pos,end,Name,Trees),
	    recorded('N'(Right))
	  ),
%	  \+ recorded( block_tree(Name,Right) ),
	  \+ recorded(OK::ok_tree(Name,Right)),
	  (  ( Trees = [] ->
	       check_close_anchor_point(Name,Right)
	     ;
	       check_first_lctag_tree(Trees,Right),
	       check_anchor_point(Name,Right)
	     ),
%%	    check_coanchor_point(Name,Right),
	    check_foot_point(Name,Right),
%%	    check_subst_point(Name,Right),
	    true
		xor fail ),
	  (recorded('N'(Right)) ->
	       true
	   ; 'C'(Right,lemma{ lex => NextLex},_),
	     (Now == no xor check_lctag_proba_lex(NextLex,Name) xor Now = no),
	     true
	  ),
	  (Now == no xor check_lctag_proba_rlex(Lex,Name) xor Now = no),
	  (Now = yes,
	   true ->
	       record(OK),
	       register_ok_tree(Right,Name)
	   ;
	   (recorded(OK) xor record(reserved_ok_tree(Name,Right)))
	  ),
	  every(( lctag_map_reverse(Name,_Cat,_Mode),
		  \+ recorded( OK_CAT::ok_cat(_Cat,_Mode,Right)),
		  record(OK_CAT)
		)),
	  fail
	;
	  true
	)
	.

%%:-std_prolog check_first_lctag_tree/1.

:-xcompiler
check_first_lctag_tree(Trees,Left) :-
	( domain(Name,Trees),
	  loaded_tree(Name),
	  check_close_anchor_point(Name,Left)
	->
	  true
	;
	  fail
	)
	.
	
:-light_tabular special_lctag/2.
:-mode(special_lctag/2,+(+,-)).

%:-std_prolog check_anchor_point/2.

:-xcompiler
check_anchor_point(Name,Left) :-
%%	recorded(loaded_tree(Name)),
	( \+ require_anchor(Name) ->
	  true
	 ; %term_range(Left,1000,_Left),
	   anchor_point(Name,_Left,Cat),
	   Left =< _Left,
	   true ->
	  true
	;
	  fail
	)
	.

:-extensional distance_constraint/2.

:-xcompiler
check_close_anchor_point(Name,Left) :-
%%	recorded(loaded_tree(Name)),
	( \+ require_anchor(Name) ->
	  true
	 ; %term_range(Left,1000,_Left),
	   anchor_point(Name,_Left,Cat),
	   Left =< _Left,
	   true ->
	  true
	;
	  fail
	)
	.



:-xcompiler
check_coanchor_point(Name,Left) :- % still unsafe
%%	recorded(loaded_tree(Name)),
	( \+ coanchor_point(Name,_) ->
	  true
	 ; %term_range(Left,1000,_Left),
	   coanchor_point(Name,_Left),
	   Left =< _Left,
	   true
	   ->
	  true
	;
	  fail
	)
	.

:-xcompiler
check_foot_point(Name,Left) :-
%%	recorded(loaded_tree(Name)),
	( \+ foot_point(Name,_) ->
	  true
	 ; %term_range(Left,1000,_Left),
	   foot_point(Name,_Left),
	   Left =< _Left,
%%	  format('foot point left=~w _left=~w name=~w\n',[Left,_Left,Name]),
	  %% foot point in left tig tree comes last (after any anchor point)
	  ( \+ require_anchor(Name)
			     xor
%          term_range(Left,_Left,_LeftAnchor),			     
	  anchor_point(Name,_LeftAnchor,_),
	    %%	    format('anchor+foot point left=~w _left=~w _leftanchor=~w name=~w\n',[Left,_Left,_LeftAnchor,Name]),
	    _LeftAnchor < _Left,
	    Left =< _LeftAnchor,
	  true
	  ),
	  true
	->
	  true
	;
	  fail
	)
	.

:-xcompiler
check_subst_point(Name,Left) :-	% still unsafe
%%	recorded(loaded_tree(Name)),
	( \+ subst_point(Name,_) ->
	  true
	 ; %term_range(Left,1000,_Left),
	   subst_point(Name,_Left),
	   Left =< _Left,
	   true
	   ->
	  true
	;
	  fail
	)
	.

correction_anchor(_).

:-std_prolog register_correction/11.

register_correction(Cat,Left,Right,Token,Info,Top,_Top,TLex,Lemma,Msg,Cycle) :-
    ( Left = (Left1:Left2) xor Left1 = Left, Left2=Left),
    (recorded(CInfo::added_correction_info(Left1,Left2,Msg,Cat,Token,_Cycle)),
     _Cycle \== Cycle,
     recorded(added_correction(K)) ->
         format('*** failed correction_anchor ~w left1=~w left2=~w cat=~w token=~w\n',[Msg,Left1,Left2,Cat,Token]),
	 erase(CInfo),
	 true
     ;
     true
    ),
    format('*** try registered correction_anchor ~w left=~w cat=~w token=~w cycle=~w K=~w\n',[Msg,Left,Cat,Token,Cycle,K]),
	( \+ added_correction,
          ( fail,
	    recorded(added_correction(K)) ->
		fail
	   ; recorded(added_minimal_correction_info(Msg,Left1,Cat)) ->
		 fail
	   ;
	   true
	  ),
	  record( added_correction ),
	  record( K:: 'C'(Left1,
			  lemma{ top => Top,
				 lex => Token,
				 cat => Cat,
				 truelex => TLex,
				 lemma => Lemma,
				 anchor => Info
			   },
			  Right
			 )
		),
%	  record(added_correction(K)),
	  record(added_correction_info(Left1,Left2,Msg,Cat,Token,Cycle)),
	  record(added_minimal_correction_info(Msg,Left1,Cat)),
	  format('+++ registered correction_anchor ~w left1=~w left2=~w cat=~w token=~w\n',[Msg,Left1,Left2,Cat,Token]),
	  %	  format('recorded new ~w\n',[K]),
	  every((%fail,
		 Lower is Left2 - 2,
		 term_range(Lower,Left1,_Left),
		 continuation_at(_Left,Addr),
		 _Left =< Left1,
		 _Left > Left2 - 4,
%		 format('try reschedule left=~w addr=~w\n',[_Left,Addr]),
		 '$reschedule'(Addr)
		))
	)
	.

%:-light_tabular continuation_at/2.
%:-mode(continuation_at/2,+(-,-)).

:-light_tabular tab_selected_item/2.
:-mode(tab_selected_item/2,+(-,-)).

tab_selected_item(Left,O) :-
    ( O = '*CITEM*'(Call,Call),
      Left = _Left
     ; O = '*SACITEM*'(Call),
       Left = _Left
     ; %fail,
     O = '*RITEM*'(Call,Ret),
     Left = _Right
    ),
    ( tab_item_term( '*RITEM*'(Call,Ret), T (_Left,_Right) )
     ; tab_item_term( '*RITEM*'(Call,Ret), T1 (_Left,_Left) * T2 (_Left,_Right) )
     ; tab_item_term( '*RITEM*'(Call,Ret), cutter(_Left,_Right,_))
     ; tab_item_term( '*RITEM*'(Call,Ret), verbose!anchor(_,_Left,_Right,_,_,_,_))
     ; tab_item_term( '*RITEM*'(Call,Ret), verbose!coanchor(_,_Left,_Right,_,_))
    )
.
		  
%:-std_prolog continuation_at/2.

:-xcompiler
continuation_at(Left,Addr) :-
    tab_selected_item(Left,O),
    recorded(O,Addr),
    %% format('found item to rechedule ~w\n',[O]),
    %% format('continuation found o=~w\n',[O]),
    true
.
